<?php

/**
 * The template for displaying product archives content, including the main shop page which is a post type archive.
 */

if (!defined('ABSPATH')) {
    exit;
}

global $nm_theme_options, $nm_globals;

$term = get_queried_object();

get_header(); ?>

<div id="prehaff-sub-category" class="prehaff page-sub-category">
    <div id="category-title" class="nm-row">
        <div class="col-md-12">
            <h2><?php echo $term->name; ?></h2>
        </div>
    </div>
    <div id="category-description" class="nm-row">
        <div class="col-md-12">
            <?php echo $term->description; ?>
        </div>
    </div>
    <div id="category-products">
        <div class="nm-row">
            <div class="col-md-12 filters">
                <div class="mobile-block">
                    <div>Hiển thị: </div>
                    <div><a href="#list" onclick="displayType('list')" class="display-type-list"><i class="fa fa-list" aria-hidden="true"></i></a></div>
                    <div><a href="#grid" onclick="displayType('grid')" class="active display-type-grid"><i class="fa fa-th" aria-hidden="true"></i></a></div>
                </div>
                <div class="mobile-block">
                    <div>Sắp xếp theo: </div>
                    <div>
                        <select name="order">
                            <option value="null">Không sắp xếp</option>
                            <option value="title:asc">Tên (A-Z)</option>
                            <option value="title:desc">Tên (Z-A)</option>
                        </select>
                    </div>
                </div>
                <div class="mobile-block">
                    <div>Hiển thị: </div>
                    <div>
                        <input name="limit" value="12" type="number" />
                    </div>
                </div>
            </div>
        </div>
        <div id="products" class="nm-row"></div>
        <div id="paging" class="nm-row">
            <div class="col-md-12 pagination paging clearfix">
                <div class="links"></div>
                <div class="results"></div>
            </div>
        </div>
        <div class="loading-indicator-wrapper">
            <div id="loading-indicator"></div>
        </div>
    </div>
</div>

<!-- Script -->
<script src="<?php echo NM_THEME_URI . '/prehaff-custom/plugins/loading_indicators/js/jajaxloader.js' ?>"></script>
<link rel="stylesheet" href="<?php echo NM_THEME_URI . '/prehaff-custom/plugins/loading_indicators/skin/jajaxloader.css' ?>">
<link rel="stylesheet" href="<?php echo NM_THEME_URI . '/prehaff-custom/plugins/loading_indicators/skin/lukehaas/tear_ball.css' ?>">

<script type="text/javascript">
    var $ = jQuery;
    var urlSite = window.location.origin;
    var urlGetProducts = urlSite + "/wp-json/prehaff/v1/filter-products";
    var p = 1;
    var n, products, total_products, total_pages = 0;
    var display_type = 'grid';
    var jTarget = $('#loading-indicator');

    function displayType(type) {
        if (display_type != type) {
            display_type = type;
            if (type == 'list') {
                $('#category-products .filters .display-type-list').addClass('active');
                $('#category-products .filters .display-type-grid').removeClass('active');
                $('#products .product-grid').addClass('product-list');
                $('#products .product-grid').removeClass('product-grid');
                $('#products .product-cols').addClass('col-fullwidth');
            } else {
                $('#category-products .filters .display-type-grid').addClass('active');
                $('#category-products .filters .display-type-list').removeClass('active');
                $('#products .product-list').addClass('product-grid');
                $('#products .product-list').removeClass('product-list');
                $('#products .product-cols').removeClass('col-fullwidth');
            }
        }

    }

    function showLoading() {
        jTarget.height($('#category-products').height());
        jTarget.ajaxloader();
        jTarget.show();
    }

    function hideLoading() {
        jTarget.ajaxloader("stop");
        jTarget.hide();
    }

    // Get Products
    function getProducts() {
        showLoading();
        n = $('input[name="limit"]').val();
        var params = {
            c: '<?php echo $term->term_id; ?>',
            n: n,
            p: p,
            o: $('select[name="order"]').val()
        };
        $.get(urlGetProducts, params).done(function(data) {
            if (data.status) {
                products = data.products.posts;
                total_products = data.total_products;
                printProducts();
            } else {
                alert(data.message);
            }
        });
    }

    function goToPage(page) {
        let current_page = p;
        switch (page) {
            case 'first':
                p = 1;
                break;
            case 'previous':
                p -= 1;
                break;
            case 'next':
                p += 1;
                break;
            case 'last':
                p = total_pages;
                break;
            default:
                p = page;
                break;
        }

        if (current_page != p) {
            getProducts();
        }
    }

    function printResult() {
        var min_show = (0 < total_products) ? ((p - 1) * n) + 1 : 0;
        var max_show = ((p * n) < total_products) ? (p * n) : total_products;
        var html = 'Hiển thị ' + min_show + ' đến ' + max_show + ' trong ' + total_products + ' (' + total_pages + ' Trang)';

        $('#paging .pagination .results').html(html);
    }

    function printPaging() {
        var html = '';
        total_pages = Math.ceil(total_products / n);
        if (n < total_products) {
            if (p > 1) {
                html += '<a href="javascript:;" onclick="goToPage(\'first\')">|&lt;</a>';
                html += '<a href="javascript:;" onclick="goToPage(\'previous\')">&lt;</a>';
            }

            for (i = 1; i <= total_pages; i++) {
                // Case current page = i
                if (p == i) {
                    html += '<b>' + i + '</b>';
                } else {
                    html += '<a href="javascript:;" onclick="goToPage(' + i + ')">' + i + '</a>';
                }
            }
            if (p < total_pages) {
                html += '<a href="javascript:;" onclick="goToPage(\'next\')">&gt;</a>';
                html += '<a href="javascript:;" onclick="goToPage(\'last\')">&gt;|</a>';
            }
        }

        $('#paging .pagination .links').html(html);
    }

    function printProducts() {
        var html = '';
        var i = 0;
        html += '<div class="product-grid"><div class="products-block">';
        products.forEach(element => {
            if (i % 4 == 0) {
                html += '<div class="nm-row product-items">';
            }
            html += prindProduct(element);
            if (i % 4 == 3) {
                html += '</div>';
            }
            i++;
        });
        html += '</div></div>';

        $('#products').html(html);
        printPaging();
        printResult();
        hideLoading();
    }

    function prindProduct(product) {
        var html = '';
        html += '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 product-cols">';
        html += '<div class="product-block">';
        html += '<div class="image">';
        html += '<a class="img" href="' + product.link + '"><img class="img-responsive" src="' + product.image + '" title="' + product.post_title + '" alt="' + product.post_title + '"></a>';
        html += '</div>';
        html += '<div class="product-meta">	';
        html += '<div class="left">';
        html += '<h3 class="name"><a href="' + product.link + '">' + product.post_title + '</a></h3>';
        html += '<div class="price">';
        html += '<span class="special-price">' + product.price + '</span>';
        html += '</div>';
        html += '</div>';
        html += '<div class="right">';
        html += '<p class="description">' + product.description + '</p>';
        html += '<div class="action">';
        html += '<div class="button-group">';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        return html;
    }

    $(document).ready(function() {
        getProducts();

        $('select[name="order"]').change(function() {
            getProducts();
        });

        $('input[name="limit"]').change(function() {
            getProducts();
        });

        jTarget.ajaxloader({
            cssClass: 'lukehaas_tear_ball'
        });

    });
</script>

<?php get_footer(); ?>