<?php

/**
 * The template for displaying product archives content, including the main shop page which is a post type archive.
 */

if (!defined('ABSPATH')) {
    exit;
}

global $nm_theme_options, $nm_globals;

$term = get_queried_object();

$sub_cats = get_terms(array(
    'taxonomy' => 'product_cat',
    'hide_empty' => 0,
    'parent' => $term->term_id
));

get_header(); ?>

<div id="prehaff-main-category" class="prehaff page-main-category">
    <div class="nm-row">
        <?php foreach ($sub_cats as $key => $value) {
            $sub_link = get_category_link($value);
            $thumbnail_id = get_woocommerce_term_meta($value->term_id, 'thumbnail_id', true);
            $image = ($thumbnail_id != 0) ? wp_get_attachment_url($thumbnail_id) : NM_THEME_URI . '/prehaff-custom/images/cat-no-image.jpg';
            ?>
            <div class="col-md-2 col-sm-3 col-xs-6 information-sgs">
                <a href="<?php echo $sub_link; ?>">
                    <img alt="" class="img-responsive" src="<?php echo $image; ?>" />
                    <?php if ($thumbnail_id == 0) { ?>
                        <div class="overflow-wrapper">
                            <h4><?php echo $value->name; ?></h4>
                        </div>
                    <?php } ?>
                </a>
            </div>
        <?php } ?>
    </div>
</div>

<?php get_footer(); ?>