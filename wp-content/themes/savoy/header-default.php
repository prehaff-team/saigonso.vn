<?php
global $nm_theme_options, $nm_globals;

// Ubermenu
if (function_exists('ubermenu')) {
    $ubermenu = true;
    $ubermenu_wrap_open = '<div class="nm-ubermenu-wrap clear">';
    $ubermenu_wrap_close = '</div>';
} else {
    $ubermenu = false;
    $ubermenu_wrap_open = $ubermenu_wrap_close = '';
}

// Layout class
$header_class = $nm_globals['header_layout'];

// Scroll class
$header_scroll_class = apply_filters('nm_header_on_scroll_class', 'resize-on-scroll'); // Note: Use "static-on-scroll" class to prevent header top/bottom spacing from resizing on-scroll
$header_class .= (strlen($header_scroll_class) > 0) ? ' ' . $header_scroll_class : '';

// Alternative logo class
$header_class .= ($nm_theme_options['alt_logo_config'] != '0') ? ' ' . $nm_theme_options['alt_logo_config'] : '';
?>

<!-- header -->
<header id="nm-header" class="nm-header <?php echo esc_attr($header_class); ?> clear">
    <div class="nm-header-inner">
        <div class="nm-header-row nm-row">
            <div class="nm-header-col col-xs-12">
                <?php echo $ubermenu_wrap_open; ?>

                <?php
                // Header part: Logo
                get_header('part-logo');
                ?>

                <?php if ($ubermenu) : ?>
                <?php ubermenu('main', array('theme_location' => 'main-menu')); ?>
                <?php else : ?>
                <nav class="nm-main-menu">
                    <ul id="nm-main-menu-ul" class="nm-menu">
                        <?php
                            wp_nav_menu(array(
                                'theme_location'    => 'main-menu',
                                'container'           => false,
                                'fallback_cb'         => false,
                                'items_wrap'          => '%3$s'
                            ));
                            ?>
                    </ul>
                </nav>
                <?php endif; ?>



                <?php echo $ubermenu_wrap_close; ?>
            </div>
        </div>
    </div>

    <?php
    // Shop search-form
    if ($nm_globals['shop_search_header']) {
        //wc_get_template( 'product-searchform_nm.php' );
        get_template_part('woocommerce/product', 'searchform_nm'); // Note: Don't use "wc_get_template()" here in case default checkout is enabled
    }
    ?>

</header>
<!-- /header -->