<?php
global $nm_theme_options, $nm_globals, $nm_body_class;

// Favicon
$custom_favicon = false;
if (!function_exists('has_site_icon') || !has_site_icon()) {
    if (isset($nm_theme_options['favicon']) && strlen($nm_theme_options['favicon']['url']) > 0) {
        $custom_favicon = true;
        $favicon_url = (is_ssl()) ? str_replace('http://', 'https://', $nm_theme_options['favicon']['url']) : $nm_theme_options['favicon']['url'];
    }
}

// Page load transition class
$nm_body_class .= ' prehaff nm-page-load-transition-' . $nm_theme_options['page_load_transition'];

// CSS animations preload class
$nm_body_class .= ' nm-preload';

// Top bar
$top_bar = $nm_theme_options['top_bar'];
$top_bar_column_left_size = intval($nm_theme_options['top_bar_left_column_size']);
$top_bar_column_right_size = 12 - $top_bar_column_left_size;
$nm_body_class .= ($top_bar) ? ' has-top-bar' : '';

// Header fixed class
$nm_body_class .= ($nm_theme_options['header_fixed']) ? ' header-fixed' : '';

if (is_front_page()) {
    // Header transparency class - Home-page
    $nm_body_class .= ($nm_theme_options['header_transparency'] != '0') ? ' header-transparency' : '';

    // Header border class - Home-page
    $nm_body_class .= (isset($_GET['header_border'])) ? ' header-border-1' : ' header-border-' . $nm_theme_options['home_header_border'];
} elseif (nm_woocommerce_activated() && (is_shop() || is_product_taxonomy())) {
    // Header transparency class - Shop
    $nm_body_class .= ($nm_theme_options['header_transparency'] == 'home-shop') ? ' header-transparency' : '';

    // Header border class - Shop
    $nm_body_class .= ' header-border-' . $nm_theme_options['shop_header_border'];
} else {
    // Header border class
    $nm_body_class .= ' header-border-' . $nm_theme_options['header_border'];
}

// Widget panel class
$nm_body_class .= ' widget-panel-' . $nm_theme_options['widget_panel_color'];

// Sticky footer class
$sticky_footer_class = ' footer-sticky-' . $nm_theme_options['footer_sticky'];

// Header: Mobile layout class
$nm_body_class .= ' header-mobile-' . $nm_theme_options['header_layout_mobile'];

// Header: Layout slug
$header_slugs = array('default' => '', 'menu-centered' => '', 'centered' => '', 'stacked' => '', 'stacked-centered' => '');
$nm_globals['header_layout'] = (isset($_GET['header']) && isset($header_slugs[$_GET['header']])) ? $_GET['header'] : $nm_theme_options['header_layout'];

// WooCommerce login
if (nm_woocommerce_activated() && !is_user_logged_in() && is_account_page()) {
    $nm_body_class .= ' nm-woocommerce-account-login';
}
?>
<!DOCTYPE html>

<html <?php language_attributes(); ?> class="<?php echo esc_attr($sticky_footer_class); ?>">

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <?php if ($nm_theme_options['custom_title']) : ?>
    <!-- Title -->
    <title><?php wp_title('&ndash;', true, 'right'); ?></title>
    <?php endif; ?>

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <?php if ($custom_favicon) : ?>
    <!-- Favicon -->
    <link href="<?php echo esc_url($favicon_url); ?>" rel="shortcut icon">
    <?php endif; ?>

    <?php wp_head(); ?>
</head>

<body <?php body_class(esc_attr($nm_body_class)); ?>>

    <?php if ($nm_theme_options['page_load_transition']) : ?>
    <div id="nm-page-load-overlay" class="nm-page-load-overlay"></div>
    <?php endif; ?>

    <!-- page overflow wrapper -->
    <div class="nm-page-overflow">

        <!-- page wrapper -->
        <div class="nm-page-wrap">

            <?php if ($top_bar) : ?>
            <!-- top bar -->

            <!-- /top bar -->
            <?php endif; ?>
            <div class="container prehaff-top-header" id="prehaff-top-header">
                <!-- BEGIN: Mobile top header -->
                <div id="i-mobile-top-header">
                    <nav class="nm-right-menu hidden-md">
                        <ul id="nm-right-menu-ul" class="nm-menu">
                            <!-- <p class="text-menu hidden-lg hidden-md hidden-sm">Danh mục</p> -->
                            <?php
                            wp_nav_menu(array(
                                'theme_location'    => 'right-menu',
                                'container'           => false,
                                'fallback_cb'         => false,
                                'items_wrap'          => '%3$s'
                            ));

                            if (nm_woocommerce_activated() && $nm_theme_options['menu_login']) :
                                ?>
                            <li class="nm-menu-account menu-item">
                                <?php echo nm_get_myaccount_link(true); ?>
                            </li>
                            <?php
                            endif;

                            if ($nm_globals['cart_link']) :

                                $cart_menu_class = ($nm_theme_options['menu_cart_icon']) ? 'has-icon' : 'no-icon';
                                $cart_url = ($nm_globals['cart_panel']) ? '#' : WC()->cart->get_cart_url();
                                ?>
                            <li class="nm-menu-cart menu-item <?php echo esc_attr($cart_menu_class); ?>">
                                <a href="<?php echo esc_url($cart_url); ?>" id="nm-menu-cart-btn">
                                    <?php echo nm_get_cart_title(); ?>
                                    <?php echo nm_get_cart_contents_count(); ?>
                                </a>
                            </li>
                            <?php
                            endif;

                            if ($nm_globals['shop_search_header']) :
                                ?>
                            <li class="nm-menu-search menu-item"><a href="#" id="nm-menu-search-btn"><i
                                        class="nm-font nm-font-search-alt flip"></i></a></li>
                            <?php endif; ?>
                            <li class="nm-menu-offscreen menu-item">
                                <?php
                                if (nm_woocommerce_activated()) {
                                    echo nm_get_cart_contents_count();
                                }
                                ?>

                                <a href="#" id="nm-mobile-menu-button" class="clicked">
                                    <div class="nm-menu-icon">
                                        <span class="line-1"></span><span class="line-2"></span><span
                                            class="line-3"></span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <a href="<?php echo esc_url(home_url('/')); ?>" class="logo-mobile">
                        <img src="<?php echo get_template_directory_uri(); ?>/prehaff-custom/images/logo.png"
                            class="nm-logo prehaff-logo" alt="<?php bloginfo('name'); ?>">
                    </a>
                    <div class="nm-shop-search-inner prehaff-search">
                        <?php //echo do_shortcode('[wpdreams_ajaxsearchpro id=1]'); 
                        ?>
                        <select id="select2-search-post" name="prehaff-search-post" class="prehaff-search-post">

                        </select>
                    </div>
                </div>
                <!-- END: Mobile top header -->
                <div class="nm-row" id="desktop-header">
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                        <div class="nm-header-logo">
                            <a href="<?php echo esc_url(home_url('/')); ?>">
                                <img src="<?php echo get_template_directory_uri(); ?>/prehaff-custom/images/logo.png"
                                    class="nm-logo prehaff-logo" alt="<?php bloginfo('name'); ?>">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
                        <!-- BEGIN: Top menu -->
                        <div class="prehaff-top-menu">
                            <ul class="prehaff-menu-custom">
                                <li class="prehaff-first"><a href="<?php echo esc_url(home_url('/')); ?>dich-vu"
                                        title="Chính sách bảo mật">Dịch vụ</a></li>
                                <li><a href="<?php echo esc_url(home_url('/')); ?>tin-tuc" title="Tin tức">Tin tức</a>
                                </li>
                                <li class="prehaff-last"><a href="<?php echo esc_url(home_url('/')); ?>gioi-thieu"
                                        title="Giới thiệu">Giới
                                        thiệu</a></li>
                            </ul>
                        </div>
                        <!-- END: Top menu -->
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div class="nm-shop-search-inner prehaff-search">
                            <?php //echo do_shortcode('[wpdreams_ajaxsearchpro id=1]'); 
                            ?>
                            <select id="select2-search-post" name="prehaff-search-post" class="prehaff-search-post">

                            </select>
                        </div>
                    </div>
                    <!-- <div class="col-sm-3">
                        <div class="tu-van hidden-xs"><div class="ic-tu-van"></div><p class="content-tu-van">TƯ VẤN HỖ TRỢ</p><p class="content-tu-van" style="font-size:18px; color: #f00e0f; font-weight: bold;">0941.33.44.55</p></div>
                    </div> -->
                </div>
            </div>
            <div class="nm-page-wrap-inner">

                <div id="nm-header-placeholder" class="nm-header-placeholder"></div>

                <?php
                // Include header layout
                if ($nm_globals['header_layout'] == 'centered') {
                    get_header('centered');
                } else {
                    get_header('default');
                }
                ?>