<?php
global $nm_theme_options, $nm_globals;

// Copyright text
$copyright_text = (isset($nm_theme_options['footer_bar_text']) && strlen($nm_theme_options['footer_bar_text']) > 0) ? $nm_theme_options['footer_bar_text'] : '';
if ($nm_theme_options['footer_bar_text_cr_year']) {
    $copyright_text = sprintf('&copy; %s %s', date('Y'), $copyright_text);
}

// Bar right-column content
if ($nm_theme_options['footer_bar_content'] !== 'social_icons') {
    $display_social_icons = false;
    $display_copyright_in_menu = ($nm_theme_options['footer_bar_content'] !== 'copyright_text') ? true : false;
    $bar_content = ($display_copyright_in_menu) ? do_shortcode($nm_theme_options['footer_bar_custom_content']) : $copyright_text;
} else {
    $display_social_icons = true;
    $display_copyright_in_menu = true;
}
?>

</div>
</div>
<!-- /page wrappers -->
<div class="hlam-alo-phone hlam-alo-green hlam-alo-show">
    <a href="tel:0941334455" title="Liên hệ tư vấn">
        <div class="hlam-alo-ph-circle"></div>
        <div class="hlam-alo-ph-circle-fill"></div>
        <div class="hlam-alo-ph-img-circle"></div>
    </a>
</div>
<div class="prehaff-float-icon prehaff-chat-fb">
    <a href="<?php echo get_option('prehaff_link_chat_facebook'); ?>" target="_blank" title="Chat ngay với chúng tôi qua facebook">
        <img src="<?php echo NM_THEME_URI . '/prehaff-custom/images/facebook-messenger.svg' ?>">
    </a>
</div>
<div id=" nm-page-overlay" class="nm-page-overlay"></div>
<div id="nm-widget-panel-overlay" class="nm-page-overlay"></div>

<!-- footer -->
<footer id="nm-footer" class="nm-footer">
    <?php
    // if (is_active_sidebar('footer')) {
    //     get_footer('widgets');
    // }
    ?>
    <div class="nm-footer-bar">
        <div class="nm-footer-bar-inner">
            <div class="nm-row">
                <!-- <div class="nm-footer-bar-left col-md-4 col-xs-12">
                    <div class="logo">
                        <a href="<?php echo esc_url(home_url('/')); ?>">
                            <img src="<?php echo get_template_directory_uri(); ?>/prehaff-custom/images/logo.png"
                                class="nm-logo" alt="<?php bloginfo('name'); ?>">
                        </a>
                    </div>
                    <div class="company-info">
                        <span>Công ty TNHH Công nghệ Sài Gòn Số</span>
                    </div>
                    <div class="sgsvn-address">
                        <span class="mr-5">Địa chỉ:</span><span>578 Đường 3/2 , P14 , Q10 , TP.HCM</span>
                    </div>
                    <div class="sgsvn-phone-number">
                        <span class="mr-5">Điện thoại:</span><span>0966.58.68.86</span>
                    </div>
                    <div class="sgsvn-email">
                        <span class="mr-5">Email:</span><span>linhkiendoc.vn@gmail.com</span>
                    </div>
                </div> -->

                <!-- <div class="nm-footer-bar-right col-md-8 col-xs-12">
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.1796990852017!2d106.6621943154184!3d10.797544992307222!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752931e23b5221%3A0x94df3d454438b5e5!2zMzI4IEzDqiBWxINuIFPhu7ksIFBoxrDhu51uZyAyLCBUw6JuIELDrG5oLCBI4buTIENow60gTWluaCwgVmlldG5hbQ!5e0!3m2!1sen!2s!4v1502763740831"
                        width="800" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div> -->
                <!-- <div class="col-md-4 col-xs-12 pt-50">
                    <h3 class="prehaff-footer-item-title">HỆ THỐNG SỬA CHỮA ĐIỆN THOẠI</h3>
                    <p class="text-footer">Chi nhánh 1:
                        13 Nguyễn Phúc Nguyên , Phường 10 , Quận 3</p>
                    <p class="text-footer">Chi nhánh 2:
                        114 Hoàng Diệu 2, Linh Xuân, Thủ Đức</p>
                    <p class="text-footer">Chi nhánh 3:
                        578 Đường 3/2, Q.10</p>
                </div>
                <div class="col-md-4 col-xs-12 pt-50">
                    <h3 class="text-footer prehaff-footer-item-title"><b>HOTLINE:</b>01666.04.04.04</h3>
                    <strong class="time-probation-title">Thời gian thử việc:</strong>
                    <div class="time-probation">THỨ 2 - THỨ 7 (8:00 AM - 19:30 PM)</div>
                    <div class="time-probation">CHỦ NHẬT (8:00 AM - 18:00 PM)</div>
                </div> -->
                <div class="col-md-4 col-sm-12">
                    <div class="footer__item clearfix">
                        <h3 class="footer__title">HỆ THỐNG CỬA HÀNG</h3>
                        <div class="footer__content">
                            <ul class="list-item">
                                <?php echo do_shortcode('[prehaff_footer_stores]'); ?>
                                <li class="address">
                                    <p>Hotline tư vấn: <a href="tel:0941334455">0941 33 44 55</a></p>
                                </li>
                                <li class="address">
                                    <p>Giờ làm việc: Thứ 2 - CN: 8h - 20h</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="footer__item clearfix">
                        <h3 class="footer__title">Thông tin hỗ trợ</h3>
                        <div class="footer__content">
                            <ul class="list-item">
                                <li class="item">
                                    <a href="<?php echo esc_url(home_url('/')); ?>/chinh-sach-bao-mat/">Chính
                                        sách bảo
                                        mật</a>
                                </li>
                                <li class="item">
                                    <a href="<?php echo esc_url(home_url('/')); ?>/chinh-sach-van-chuyen/">Chính
                                        sách
                                        vận chuyển</a>
                                </li>
                                <li class="item">
                                    <a href="<?php echo esc_url(home_url('/')); ?>/chinh-sach-bao-hanh/">Chính
                                        sách bảo
                                        hành</a>
                                </li>
                                <li class="item">
                                    <a href="<?php echo esc_url(home_url('/')); ?>/huong-dan-thanh-toan/">Hướng
                                        dẫn
                                        thanh toán</a>
                                </li>
                                <li class="item">
                                    <a href="<?php echo esc_url(home_url('/')); ?>/gioi-thieu">Giới thiệu
                                        chung</a>
                                </li>
                                <li class="item">
                                    <a href="<?php echo esc_url(home_url('/')); ?>/tuyen-dung/">Thông tin tuyển
                                        dụng</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="footer__item clearfix">
                        <h3 class="footer__title">Thông tin hỗ trợ</h3>
                        <div class="footer__content">
                            <!-- Faceobook fan page -->
                            <div class="fb-page" data-href="https://www.facebook.com/saigonso.vn/" data-height="214" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                                <blockquote class="fb-xfbml-parse-ignore" cite="https://www.facebook.com/saigonso.vn/">
                                    <a href="https://www.facebook.com/saigonso.vn/">Sài Gòn Số</a></blockquote>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</footer>
<!-- /footer -->
<div class="prehaff-copyright">
    <div class="container">
        <div class="nm-row">
            <div class="col-md-12">
                <div class="pull-left">
                    <h3 class="copyright-title">Copyright © 2019 - SAIGONSO.VN</h3>
                    <span class="description">
                        Địa chỉ: 578 Đường 3/2 , P14 , Q10 , TP.HCM<br>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- BEGIN: Footer actions -->
<div class="action-footer">
    <div class="container">
        <div class="menu-direct prehaff-sidebar-block">
            <div class="btn-close-direct">
                <a href="javacript:;">
                    <i class="fa fa-times"></i>
                </a>
            </div>
            <?php echo do_shortcode('[prehaff_sidebar_stores]'); ?>
        </div>
    </div>
    <div class="nm-row">
        <div class="col-sm-4">
            <div class="c-call-now action-footer-item">
                <a href="tel:0941334455" title="Gọi cho chúng tôi để được tư vấn miễn phí"><i class="fa fa-phone" aria-hidden="true"></i> <span>Gọi điện</span></a>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="c-messager action-footer-item">
                <a href="<?php echo get_option('prehaff_link_chat_facebook'); ?>" target="_blank" title="Chat ngay với chúng tôi qua facebook">
                    <img src="<?php echo NM_THEME_URI . '/prehaff-custom/images/facebook-messenger.svg' ?>"> <span>Nhắn tin</span>
                </a>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="c-address action-footer-item">
                <a href="javascript:;" title="Chỉ đường" class="c-btn-direct"><i class="fa fa-map-marker"></i></i>
                    <span>Chỉ đường</span></a>
            </div>
        </div>
    </div>
</div>
</div>
<!-- END: Footer actions -->
<!-- mobile menu -->
<div id="nm-mobile-menu" class="nm-mobile-menu">
    <div class="nm-mobile-menu-scroll">
        <div class="nm-mobile-menu-content">
            <div class="nm-row">

                <div class="nm-mobile-menu-top col-xs-12">
                    <ul id="nm-mobile-menu-top-ul" class="menu">
                        <?php if ($nm_globals['cart_link']) : ?>
                            <li class="nm-mobile-menu-item-cart menu-item">
                                <a href="<?php echo esc_url(WC()->cart->get_cart_url()); ?>" id="nm-mobile-menu-cart-btn">
                                    <?php echo nm_get_cart_title(); ?>
                                    <?php echo nm_get_cart_contents_count(); ?>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php if ($nm_globals['shop_search_header']) : ?>
                            <li class="nm-mobile-menu-item-search menu-item">
                                <form role="search" method="get" action="<?php echo esc_url(home_url('/')); ?>">
                                    <input type="text" id="nm-mobile-menu-shop-search-input" class="nm-mobile-menu-search" autocomplete="off" value="" name="s" placeholder="<?php esc_attr_e('Search Products', 'woocommerce'); ?>" />
                                    <span class="nm-font nm-font-search-alt"></span>
                                    <input type="hidden" name="post_type" value="product" />
                                </form>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>

                <div class="nm-mobile-menu-main col-xs-12">
                    <ul id="nm-mobile-menu-main-ul" class="menu">
                        <?php
                        // Main menu
                        wp_nav_menu(array(
                            'theme_location'    => 'main-menu',
                            'container'           => false,
                            'fallback_cb'         => false,
                            'after'              => '<span class="nm-menu-toggle"></span>',
                            'items_wrap'          => '%3$s'
                        ));

                        // Right menu                        
                        wp_nav_menu(array(
                            'theme_location'    => 'right-menu',
                            'container'           => false,
                            'fallback_cb'         => false,
                            'after'              => '<span class="nm-menu-toggle"></span>',
                            'items_wrap'          => '%3$s'
                        ));
                        ?>
                    </ul>
                </div>

                <div class="nm-mobile-menu-secondary col-xs-12">
                    <ul id="nm-mobile-menu-secondary-ul" class="menu">
                        <?php
                        // Top bar menu
                        if ($nm_theme_options['top_bar']) {
                            wp_nav_menu(array(
                                'theme_location'    => 'top-bar-menu',
                                'container'           => false,
                                'fallback_cb'         => false,
                                'after'              => '<span class="nm-menu-toggle"></span>',
                                'items_wrap'          => '%3$s'
                            ));
                        }
                        ?>
                        <?php if (nm_woocommerce_activated() && $nm_theme_options['menu_login']) : ?>
                            <li class="nm-menu-item-login menu-item">
                                <?php echo nm_get_myaccount_link(false); ?>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- /mobile menu -->

<?php if ($nm_globals['cart_panel']) : ?>
    <!-- widget panel -->
    <div id="nm-widget-panel" class="nm-widget-panel">
        <div class="nm-widget-panel-inner">
            <div class="nm-widget-panel-header">
                <div class="nm-widget-panel-header-inner">
                    <a href="#" id="nm-widget-panel-close">
                        <span class="nm-cart-panel-title"><?php esc_html_e('Cart', 'woocommerce'); ?> <span class="nm-menu-cart-count count"><?php echo WC()->cart->get_cart_contents_count(); ?></span></span>
                        <span class="nm-widget-panel-close-title"><?php esc_html_e('Close', 'woocommerce'); ?></span>
                    </a>
                </div>
            </div>

            <div class="widget_shopping_cart_content">
                <?php woocommerce_mini_cart(); ?>
            </div>
        </div>
    </div>
    <!-- /widget panel -->
<?php endif; ?>

<?php
if ($nm_globals['login_popup'] && !is_user_logged_in() && !is_account_page()) :
    nm_add_page_include('login-popup');
    ?>
    <!-- login popup -->
    <div id="nm-login-popup-wrap" class="nm-login-popup-wrap mfp-hide">
        <?php wc_get_template('myaccount/form-login.php', array('is_popup' => true)); ?>
    </div>
    <!-- /login popup -->
<?php endif; ?>

<!-- quickview -->
<div id="nm-quickview" class="clearfix"></div>
<!-- /quickview -->

<?php if (strlen($nm_theme_options['custom_js']) > 0) : ?>
    <!-- Custom Javascript -->
    <script type="text/javascript">
        <?php echo $nm_theme_options['custom_js']; ?>
    </script>
<?php endif; ?>

<?php
// WordPress footer hook
wp_footer();
?>

</div>
<!-- /page overflow wrapper -->

</body>
<div id="fb-root"></div>
<script>
    // THINH comment
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8&appId=509014585974509";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

<script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-67056399-1', 'auto');
    ga('send', 'pageview');
</script>
<!--Start of Tawk.to Script-->
<!-- THINH Comment code show chat box #####-->
<script type="text/javascript">
    // var Tawk_API = Tawk_API || {},
    //     Tawk_LoadStart = new Date();
    // (function() {
    //     var s1 = document.createElement("script"),
    //         s0 = document.getElementsByTagName("script")[0];
    //     s1.async = true;
    //     s1.src = 'https://embed.tawk.to/584b481f8a20fc0cac50208b/default';
    //     s1.charset = 'UTF-8';
    //     s1.setAttribute('crossorigin', '*');
    //     s0.parentNode.insertBefore(s1, s0);
    //     console.log('222');

    // })();
</script>

<!-- Histats.com  (div with counter) -->
<div id="histats_counter"></div>
<!-- Histats.com  START  (aync)-->
<script type="text/javascript">
    var _Hasync = _Hasync || [];
    _Hasync.push(['Histats.start', '1,3679855,4,1,120,40,00010000']);
    _Hasync.push(['Histats.fasi', '1']);
    _Hasync.push(['Histats.track_hits', '']);
    (function() {
        var hs = document.createElement('script');
        hs.type = 'text/javascript';
        hs.async = true;
        hs.src = ('//s10.histats.com/js15_as.js');
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
    })();
</script>
<noscript><a href="/" target="_blank"><img src="//sstatic1.histats.com/0.gif?3679855&101" alt="" border="0"></a></noscript>
<!-- Histats.com  END  -->
<!--End of Tawk.to Script-->

</html>