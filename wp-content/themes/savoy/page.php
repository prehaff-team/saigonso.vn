<?php
// Only adding the "entry-content" post class on non-woocommerce pages to avoid CSS conflicts
$post_class = (nm_is_woocommerce_page()) ? '' : 'entry-content';

get_header(); ?>

<!-- <div class="nm-row">
    <div class="col-xs-12"> -->
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php if (get_the_ID() == 4) { ?>

<div class="container">
    <div id="post-<?php the_ID(); ?>" <?php post_class($post_class); ?>>
        <?php //the_content();
                    ?>
        <!-- BEGIN: Banner slider -->
        <div class="prehaff-slider-wrap">
            <div class="prehaff-slider-banner owl-carousel owl-theme">
                <div class="slider-item ">
                    <img style="width: 100%;" src="<?php echo NM_THEME_URI; ?>/prehaff-custom/images/banner-min.jpg"
                        alt="">
                </div>
                <div class="slider-item ">
                    <img style="width: 100%;" src="<?php echo NM_THEME_URI; ?>/prehaff-custom/images/thay-vo.jpg"
                        alt="">
                </div>
                <div class="slider-item ">
                    <img style="width: 100%;" src="<?php echo NM_THEME_URI; ?>/prehaff-custom/images/iphone.jpg" alt="">
                </div>
                <div class="slider-item ">
                    <img style="width: 100%;"
                        src="<?php echo NM_THEME_URI; ?>/prehaff-custom/images/unlock-dien-thoai-min.jpg" alt="">
                </div>
            </div>
            <div class="slider-nav">
                <a href="#" class="nav-btn previous round"><i class="fa fa-chevron-left"></i></a>
                <a href="#" class="nav-btn next round"><i class="fa fa-chevron-right"></i></a>
            </div>
        </div>
        <!-- END: Banner slider -->
        <!-- BEGIN: List cate -->
        <div class="list-cate">
            <div class="nm-row">
                <div class="col-md-12">
                    <h2 class="page-subheading"></h2>
                    <div class="navaccessories">
                        <a href="<?php echo get_site_url(); ?>/thay-man-hinh">
                            <div>
                                <img src="<?php echo NM_THEME_URI; ?>/prehaff-custom/images/tmh.png">
                            </div>
                            <h3>Thay màn hình</h3>
                        </a>
                        <a href="<?php echo get_site_url(); ?>/thay-pin-dien-thoai/">
                            <div>
                                <img src="<?php echo NM_THEME_URI; ?>/prehaff-custom/images/tp.png">
                            </div>
                            <h3>Thay pin</h3>
                        </a>
                        <a href="<?php echo get_site_url(); ?>/thay-vo-dien-thoai/">
                            <div>
                                <img src="<?php echo NM_THEME_URI; ?>/prehaff-custom/images/tv.png">
                            </div>
                            <h3>Thay vỏ</h3>
                        </a>
                        <a href="<?php echo get_site_url(); ?>/sua-chua">
                            <div>
                                <img src="<?php echo NM_THEME_URI; ?>/prehaff-custom/images/sdt.png">
                            </div>
                            <h3>Sửa điện thoại</h3>
                        </a>
                        <a href="<?php echo get_site_url(); ?>/chay-phan-mem">
                            <div>
                                <img src="<?php echo NM_THEME_URI; ?>/prehaff-custom/images/dv.png">
                            </div>
                            <h3>Chạy phần mềm</h3>
                        </a>
                        <a href="<?php echo get_site_url(); ?>/mo-khoa-unlock-dien-thoai">
                            <div>
                                <img src="<?php echo NM_THEME_URI; ?>/prehaff-custom/images/smtb.png">
                            </div>
                            <h3>Mở khóa<br />Unlock điện thoại </h3>
                        </a>
                        <a href="<?php echo get_site_url(); ?>/linh-kien-dien-thoai">
                            <div>
                                <img src="<?php echo NM_THEME_URI; ?>/prehaff-custom/images/tmk.png">
                            </div>
                            <h3>Link kiện
                            </h3>
                        </a>
                        <a href="<?php echo get_site_url(); ?>/thay-camera-dien-thoai/">
                            <div>
                                <img src="<?php echo NM_THEME_URI; ?>/prehaff-custom/images/tcmr.png">
                            </div>
                            <h3>Thay Camera</h3>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: List cate -->
        <!-- BEGIN: Services -->
        <!-- Set up your HTML -->
        <section id="service-section">
            <div class="nm-row">
                <div class="col-md-12">
                    <h2 class="title">DỊCH VỤ NỔI BẬT</h2>
                    <div class="stars-line">
                    </div>
                </div>
            </div>
            <div class="nm-row">
                <div id="service-slider-wrap">
                    <div class="owl-carousel" id="sgs-service">
                        <?php
                                    //---> Get data from database
                                    $args = array(
                                        'post_type' => 'product',
                                        'posts_per_page' => '10',
                                        'post_limits' => 10,
                                        // 'orderby' => 'news_published_dt',
                                        // 'order' => 'DESC'
                                    );
                                    $lastest_services = new WP_Query($args);
                                    if ($lastest_services->have_posts()) : while ($lastest_services->have_posts()) : $lastest_services->the_post();
                                            ?>
                        <div class="service-item">
                            <a href="<?php echo get_permalink($post->ID); ?>" title="<?php the_title_attribute(); ?>">
                                <div class="img-wrap">
                                    <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail'); ?>
                                    <img class="service-img" src="<?php echo $image[0]; ?>"
                                        alt="<?php the_title_attribute(); ?>" />
                                </div>
                                <h3><?php the_title_attribute(); ?></h3>
                                <div class="contact-link">
                                    <strong>Liên hệ</strong>
                                </div>
                            </a>
                        </div>
                        <?php
                                        endwhile;
                                    endif;
                                    wp_reset_postdata();
                                    ?>

                    </div>
                    <div class="owl-controls" id="service-action">
                        <div class="owl-buttons">
                            <div class="owl-prev" id="btn-prev">‹</div>
                            <div class="owl-next" id="btn-next">›</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END: Services -->
        <!-- BEGIN: Why choose us -->
        <div class="why-choose-us">
            <div class="nm-row">
                <div class="col-md-12">
                    <h2 class="title">TẠI SAO BẠN NÊN CHỌN SAIGONSO</h2>
                    <div class="stars-line">
                    </div>
                </div>
            </div>
            <div class="nm-row">
                <div class="col-sm-4 col-md-4 col-xs-12">
                    <div class="choose-sgs-left">
                        <div class="box-choose-left">
                            <div class="image-left-content"><img alt="" height="71"
                                    src="<?php echo NM_THEME_URI; ?>/prehaff-custom/images/Group.png" width="71"></div>

                            <div class="experence left-content">
                                <h3 class="title-left-content">Kinh nghiệm</h3>

                                <p>Với nhiều năm kinh nghiệm<br>
                                    sửa chữa điện thoại</p>
                            </div>
                        </div>

                        <div class="box-choose-left">
                            <div class="image-left-content"><img alt="" height="71"
                                    src="<?php echo NM_THEME_URI; ?>/prehaff-custom/images/Group-1.png" width="71">
                            </div>

                            <div class="experence left-content">
                                <h3 class="title-left-content">Giá tốt</h3>

                                <p>Giá dịch vụ thay màn hình<br>
                                    mặt kính tốt nhất trên thị trường</p>
                            </div>
                        </div>

                        <div class="box-choose-left">
                            <div class="image-left-content"><img alt="" height="71"
                                    src="<?php echo NM_THEME_URI; ?>/prehaff-custom/images/Group-2.png" width="71">
                            </div>

                            <div class="experence left-content">
                                <h3 class="title-left-content">Chất lượng cao</h3>

                                <p>Dịch vụ thay thế mới 100%<br>
                                    chất lượng cao</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-4 col-xs-12">
                    <div class="choose-sgs-center"><img alt="" class=""
                            src="<?php echo NM_THEME_URI; ?>/prehaff-custom/images/tai-sao-nen-chon.png"></div>
                </div>
                <div class="col-sm-4 col-md-4 col-xs-12">
                    <div class="choose-sgs-right">
                        <div class="box-choose-right">
                            <div class="image-right-content"><img alt="" class="img-responsive" height="71"
                                    src="<?php echo NM_THEME_URI; ?>/prehaff-custom/images/Group-3.png" width="71">
                            </div>
                            <div class="experence right-content">
                                <h3 class="title-left-content">Bảo hành</h3>
                                <p>Chính sách khuyến mãi<br>
                                    và bảo hành dài hạn</p>
                            </div>
                        </div>
                        <div class="box-choose-right">
                            <div class="image-right-content"><img alt="" height="71"
                                    src="<?php echo NM_THEME_URI; ?>/prehaff-custom/images/Group-4.png" width="71">
                            </div>
                            <div class="experence right-content">
                                <h3 class="title-left-content">Đội ngũ chuyên nghiệp</h3>

                                <p>Nhân viên làm việc nhiệt tình<br>
                                    tay nghề cao luôn vui vẻ với khách hàng</p>
                            </div>
                        </div>
                        <div class="box-choose-right">
                            <div class="image-right-content"><img alt="" height="71"
                                    src="<?php echo NM_THEME_URI; ?>/prehaff-custom/images/Group-5.png" width="71">
                            </div>
                            <div class="experence right-content">
                                <h3 class="title-left-content">Thay lấy ngay</h3>
                                <p>Thời gian thay chỉ từ<br>
                                    30 - 60 phút</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: Why choose us -->
        <!-- BEGIN: Post list -->
        <div class="list-post">
            <div class="nm-row">
                <div class="col-md-12">
                    <h2 class="title">tin tức công nghệ</h2>
                    <div class="stars-line">
                    </div>
                </div>
            </div>
            <div class="nm-row">
                <div class="col-md-6">
                    <?php echo do_shortcode('[su_posts template="templates/default-loop.php" posts_per_page="3" tax_operator="0" order="desc" orderby="modified"]'); ?>
                </div>
                <div class="col-md-6">
                    <?php echo do_shortcode('[su_posts template="templates/default-loop.php" posts_per_page="3" tax_operator="0" order="desc" orderby="modified" offset="3"]'); ?>
                </div>
            </div>
        </div>
        <!-- END: Post list -->
    </div>
</div>

<?php } else { ?>
<div class="nm-row">
    <div class="col-xs-12">
        <div id="post-<?php the_ID(); ?>" <?php post_class($post_class); ?>>
            <?php the_content(); ?>
        </div>
    </div>
</div>
<?php } ?>

<?php
    endwhile;
else :
    ?>

<div>
    <h2><?php esc_html_e('Sorry, nothing to display.', 'nm-framework'); ?></h2>
</div>

<?php endif; ?>
<!-- </div>
</div> -->

<?php
// If comments are open or we have at least one comment, load up the comment template
if (comments_open() || '0' != get_comments_number()) :
    ?>
<div class="nm-comments">
    <div class="nm-row">
        <div class="col-xs-12">
            <?php
                // Comments
                comments_template('', true);
                edit_post_link();
                ?>
        </div>
    </div>
</div>
<?php endif; ?>

<?php get_footer(); ?>