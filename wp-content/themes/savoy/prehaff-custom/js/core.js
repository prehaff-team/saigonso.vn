jQuery(function() {
    const $ = jQuery;
    var urlSite = window.location.origin;
    var urlSearchProducts = urlSite + "/wp-json/prehaff-api/v1/search";
    // your code goes here
    const owlSlider = jQuery(".prehaff-slider-banner").owlCarousel({
        items: 1,
        loop: true,
        autoplay: true
    });
    jQuery(".slider-nav .next").click(function() {
        owlSlider.trigger("next.owl.carousel");
    });
    // Go to the previous item
    jQuery(".slider-nav .previous").click(function() {
        owlSlider.trigger("prev.owl.carousel");
    });

    jQuery(".prehaff-search-post").select2({
        ajax: {
            url: urlSearchProducts,
            dataType: "json",
            delay: 500, // wait 250 milliseconds before triggering the request
            data: function(params) {
                var queryParameters = {
                    s: params.term
                };

                return queryParameters;
            },
            processResults: function(data, params) {
                return {
                    results: data
                };
            },
            cache: true
        },
        placeholder: "Tìm sản phẩm",
        language: {
            searching: function() {
                return "Tìm kiếm ...";
            },
            noResults: function() {
                return "Không tìm thấy dữ liệu phù hợp";
            }
        },
        formatResult: formatRepoSelection,
        formatSelection: formatRepoSelection,
        escapeMarkup: function(m) {
            return m;
        }
    });

    jQuery(".select2-selection__arrow").html(`<i class="fa fa-search"></i>`);

    jQuery(".prehaff-search-post").on("select2:select", function(e) {
        const linkToPost = e.params.data.link;
        window.location.assign(linkToPost);
    });

    function formatRepoSelection(repo) {
        return `<a class="info" href="${
      repo.link
    }" target="_blank">${repo.text}</a>`;
    }

    const owlServiceSlider = jQuery("#sgs-service").owlCarousel({
        items: 4,
        loop: true,
        autoplay: false,
        margin: 10,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 1
            },
            767: {
                items: 2
            },
            991: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    });

    jQuery("#service-action #btn-prev").click(function() {
        owlServiceSlider.trigger("next.owl.carousel");
    });
    // Go to the previous item
    jQuery("#service-action #btn-next").click(function() {
        owlServiceSlider.trigger("prev.owl.carousel");
    });

    jQuery(".c-btn-direct").on("click", evt => {
        evt.preventDefault();
        jQuery(".menu-direct").toggleClass("show");
    });

    const header = document.getElementById("prehaff-top-header");
    const sticky = header.offsetTop;

    function setFixTopHeader() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
            $("#i-mobile-top-header")
                .find(".prehaff-search")
                .css("display", "none");
        } else {
            header.classList.remove("sticky");
            $("#i-mobile-top-header")
                .find(".prehaff-search")
                .css("display", "block");
        }
    }

    window.onscroll = function() {
        setFixTopHeader();
    };

    $("#nm-mobile-menu-button").on("click", evt => {
        evt.preventDefault();
        if (!$("body").hasClass("mobile-menu-open")) {
            const btnClose = `<a href="#" id="nm-mobile-menu-close-button" class="">
                            <div class="nm-menu-icon">
                                <span class="line-1"></span><span class="line-2"></span><span class="line-3"></span>
                            </div>
                        </a>`;

            $("#nm-mobile-menu").prepend(btnClose);
        } else {}
    });

    $(document).on("click", "#nm-mobile-menu-close-button", evt => {
        evt.preventDefault();
        $("body").removeClass("mobile-menu-open");
        $("#nm-page-overlay").removeClass("show");
        $("#nm-mobile-menu")
            .find("#nm-mobile-menu-close-button")
            .remove();
    });

    $(document).on("click", ".btn-close-direct a", evt => {
        evt.preventDefault();
        $(".menu-direct").removeClass("show");
    });

    var urlSite = window.location.origin;
    // Load default image if not exist
    $(".su-post-thumbnail img").error(function() {
        $(this).attr(
            "srcset",
            urlSite +
            "/wp-content/themes/savoy/prehaff-custom/images/no-post-image.png"
        );
    });

    const showTawk = () => {
        var Tawk_API = Tawk_API || {},
            Tawk_LoadStart = new Date();
        (function() {
            var s1 = document.createElement("script"),
                s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = "https://embed.tawk.to/584b481f8a20fc0cac50208b/default";
            s1.charset = "UTF-8";
            s1.setAttribute("crossorigin", "*");
            s0.parentNode.insertBefore(s1, s0);

            $('#tawkchat-container iframe:first-child').
            contents().find("head").append(
                $("<style type='text/css'>" +
                    "div#tawkchat-minified-wrapper {" +
                    "bottom: 30px!important;" +
                    "} ")
            );
        })();
    };

    // if ($(window).width() > 768) {
    showTawk();
    // } else {
    //   $("#tawkchat-container").remove();
    // }

    // $(window).resize(() => {
    //   if ($(window).width() > 768) {
    //     showTawk();
    //   } else {
    //     $("#tawkchat-container").remove();
    //   }
    // });
});