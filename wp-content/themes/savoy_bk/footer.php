<?php 
	global $nm_theme_options, $nm_globals;
	
	// Copyright text
	$copyright_text = ( isset( $nm_theme_options['footer_bar_text'] ) && strlen( $nm_theme_options['footer_bar_text'] ) > 0 ) ? $nm_theme_options['footer_bar_text'] : '';
	if ( $nm_theme_options['footer_bar_text_cr_year'] ) {
		$copyright_text = sprintf( '&copy; %s %s', date( 'Y' ), $copyright_text );
	}
	
	// Bar right-column content
	if ( $nm_theme_options['footer_bar_content'] !== 'social_icons' ) {
		$display_social_icons = false;
		$display_copyright_in_menu = ( $nm_theme_options['footer_bar_content'] !== 'copyright_text' ) ? true : false;
		$bar_content = ( $display_copyright_in_menu ) ? do_shortcode( $nm_theme_options['footer_bar_custom_content'] ) : $copyright_text;
	} else {
		$display_social_icons = true;
		$display_copyright_in_menu = true;
	}
?>                

                </div>
            </div>
            <!-- /page wrappers -->
            <div class="hlam-alo-phone hlam-alo-green hlam-alo-show">
				<a href="tel:0941334455" title="Liên hệ tư vấn">
				  <div class="hlam-alo-ph-circle"></div>
				  <div class="hlam-alo-ph-circle-fill"></div>
				  <div class="hlam-alo-ph-img-circle"></div>
				</a>
			</div>
            <div id="nm-page-overlay" class="nm-page-overlay"></div>
            <div id="nm-widget-panel-overlay" class="nm-page-overlay"></div>
            
            <!-- footer -->
            <footer id="nm-footer" class="nm-footer">
                <?php
                    if ( is_active_sidebar( 'footer' ) ) {
                        get_footer( 'widgets' );
                    }
                ?>
                
                <div class="nm-footer-bar">
                    <div class="nm-footer-bar-inner">
                        <div class="nm-row">
                            <div class="nm-footer-bar-left col-md-4 col-xs-12">
                            <h3>LIÊN HỆ VỚI SAIGONSO</h3>
                            <p>Trung tâm sửa chữa điện thoại Saigonso</p>
                            <p class="text-footer"><b>Chi nhánh 1:</b>
                                13 Nguyễn Phúc Nguyên , Phường 10 , Quận 3</p>
                          <p class="text-footer"><b>Chi nhánh 2:</b>
                                114 Hoàng Diệu 2, Linh Xuân, Thủ Đức</p>
                                <p class="text-footer"><b>Chi nhánh 3:</b>
                                578 Đường 3/2, Q.10</p> 
                                <p class="text-footer"><b>HOTLINE:</b>01666.04.04.04</p>
                            </div>
                            
                            <div class="nm-footer-bar-right col-md-8 col-xs-12">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.1796990852017!2d106.6621943154184!3d10.797544992307222!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752931e23b5221%3A0x94df3d454438b5e5!2zMzI4IEzDqiBWxINuIFPhu7ksIFBoxrDhu51uZyAyLCBUw6JuIELDrG5oLCBI4buTIENow60gTWluaCwgVmlldG5hbQ!5e0!3m2!1sen!2s!4v1502763740831" width="800" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>

                        </div>
                    </div>
                </div>
            </footer>
            <!-- /footer -->
            
            <!-- mobile menu -->
            <div id="nm-mobile-menu" class="nm-mobile-menu">
                <div class="nm-mobile-menu-scroll">
                    <div class="nm-mobile-menu-content">
                        <div class="nm-row">
                                                    
                            <div class="nm-mobile-menu-top col-xs-12">
                                <ul id="nm-mobile-menu-top-ul" class="menu">
                                    <?php if ( $nm_globals['cart_link'] ) : ?>
                                    <li class="nm-mobile-menu-item-cart menu-item">
                                        <a href="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>" id="nm-mobile-menu-cart-btn">
                                            <?php echo nm_get_cart_title(); ?>
                                            <?php echo nm_get_cart_contents_count(); ?>
                                        </a>
                                    </li>
                                    <?php endif; ?>
                                    <?php if ( $nm_globals['shop_search_header'] ) : ?>
                                    <li class="nm-mobile-menu-item-search menu-item">
                                        <form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                                            <input type="text" id="nm-mobile-menu-shop-search-input" class="nm-mobile-menu-search" autocomplete="off" value="" name="s" placeholder="<?php esc_attr_e( 'Search Products', 'woocommerce' ); ?>" />
                                            <span class="nm-font nm-font-search-alt"></span>
                                            <input type="hidden" name="post_type" value="product" />
                                        </form>
                                    </li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                             
                            <div class="nm-mobile-menu-main col-xs-12">
                                <ul id="nm-mobile-menu-main-ul" class="menu">
                                    <?php
                                        // Main menu
                                        wp_nav_menu( array(
                                            'theme_location'	=> 'main-menu',
                                            'container'       	=> false,
                                            'fallback_cb'     	=> false,
                                            'after' 	 		=> '<span class="nm-menu-toggle"></span>',
                                            'items_wrap'      	=> '%3$s'
                                        ) );
                                        
                                        // Right menu                        
                                        wp_nav_menu( array(
                                            'theme_location'	=> 'right-menu',
                                            'container'       	=> false,
                                            'fallback_cb'     	=> false,
                                            'after' 	 		=> '<span class="nm-menu-toggle"></span>',
                                            'items_wrap'      	=> '%3$s'
                                        ) );
                                    ?>
                                </ul>
                            </div>
        
                            <div class="nm-mobile-menu-secondary col-xs-12">
                                <ul id="nm-mobile-menu-secondary-ul" class="menu">
                                    <?php
                                        // Top bar menu
                                        if ( $nm_theme_options['top_bar'] ) {
                                            wp_nav_menu( array(
                                                'theme_location'	=> 'top-bar-menu',
                                                'container'       	=> false,
                                                'fallback_cb'     	=> false,
                                                'after' 	 		=> '<span class="nm-menu-toggle"></span>',
                                                'items_wrap'      	=> '%3$s'
                                            ) );
                                        }
                                    ?>
                                    <?php if ( nm_woocommerce_activated() && $nm_theme_options['menu_login'] ) : ?>
                                    <li class="nm-menu-item-login menu-item">
                                        <?php echo nm_get_myaccount_link( false ); ?>
                                    </li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        
                        </div>
                    </div>
                </div>
            </div>
            <!-- /mobile menu -->
            
            <?php if ( $nm_globals['cart_panel'] ) : ?>
            <!-- widget panel -->                
            <div id="nm-widget-panel" class="nm-widget-panel">
                <div class="nm-widget-panel-inner">
                    <div class="nm-widget-panel-header">
                        <div class="nm-widget-panel-header-inner">
                            <a href="#" id="nm-widget-panel-close">
                                <span class="nm-cart-panel-title"><?php esc_html_e( 'Cart', 'woocommerce' ); ?> <span class="nm-menu-cart-count count"><?php echo WC()->cart->get_cart_contents_count(); ?></span></span>
                                <span class="nm-widget-panel-close-title"><?php esc_html_e( 'Close', 'woocommerce' ); ?></span>
                            </a>
                        </div>
                    </div>
                    
                    <div class="widget_shopping_cart_content">
                        <?php woocommerce_mini_cart(); ?>
                    </div>
                </div>
            </div>
            <!-- /widget panel -->
            <?php endif; ?>
            
            <?php
				if ( $nm_globals['login_popup'] && ! is_user_logged_in() && ! is_account_page() ) :
					nm_add_page_include( 'login-popup' );
			?>
				<!-- login popup -->
                <div id="nm-login-popup-wrap" class="nm-login-popup-wrap mfp-hide">
                    <?php wc_get_template( 'myaccount/form-login.php', array( 'is_popup' => true ) ); ?>
				</div>
                <!-- /login popup -->
			<?php endif; ?>
            
            <!-- quickview -->
            <div id="nm-quickview" class="clearfix"></div>
            <!-- /quickview -->
            
            <?php if ( strlen( $nm_theme_options['custom_js'] ) > 0 ) : ?>
            <!-- Custom Javascript -->
            <script type="text/javascript">
                <?php echo $nm_theme_options['custom_js']; ?>
            </script>
            <?php endif; ?>
            
            <?php
                // WordPress footer hook
                wp_footer();
            ?>
        
        </div>
        <!-- /page overflow wrapper -->
    	
	</body>
    <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.8&appId=509014585974509";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-67056399-1', 'auto');
  ga('send', 'pageview');

</script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/584b481f8a20fc0cac50208b/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>

<!-- Histats.com  (div with counter) --><div id="histats_counter"></div>
<!-- Histats.com  START  (aync)-->
<script type="text/javascript">var _Hasync= _Hasync|| [];
_Hasync.push(['Histats.start', '1,3679855,4,1,120,40,00010000']);
_Hasync.push(['Histats.fasi', '1']);
_Hasync.push(['Histats.track_hits', '']);
(function() {
var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
hs.src = ('//s10.histats.com/js15_as.js');
(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
})();</script>
<noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?3679855&101" alt="" border="0"></a></noscript>
<!-- Histats.com  END  -->
<!--End of Tawk.to Script-->
</html>
