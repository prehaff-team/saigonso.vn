<?php
/**
 *	The template for displaying the shop header
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $nm_theme_options, $nm_globals;

$header_class = '';
$filters_or_search_enabled = false;

// Categories
if ( $nm_theme_options['shop_categories'] ) {
    nm_add_page_include( 'shop_categories' );
	
    $show_categories = true;
} else {
	$show_categories = false;
	$header_class .= '';
}

// Filters
if ( $nm_theme_options['shop_filters'] == 'header' ) {
    nm_add_page_include( 'shop_filters' );
    
	$show_filters = true;
    $filters_or_search_enabled = true;
} else {
	$show_filters = false;
	$header_class .= ' no-filters';
}

// Search
if ( $nm_globals['shop_search'] ) {
	$filters_or_search_enabled = true;
} else {
    $header_class .= ' no-search';
}


if ( $nm_globals['shop_filters_popup'] || ! $filters_or_search_enabled ) {
    $header_class .= ' centered'; // Add "centered" class to center category-menu when filters and search is disabled
}
?>

