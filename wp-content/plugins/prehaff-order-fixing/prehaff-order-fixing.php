<?php

/**
 * Plugin Name: PREHAFF Order Fixing
 * Plugin URI: https://bitbucket.org/prehaff-team/
 * Description: This is a plugin for management order product.
 * Version: 1.0 
 * Author: PREHAFF Team
 * Author URI: 
 * License: GPLv2 or later 
 */
?>
<?php

// Make sure we don't expose any info if called directly
if (!defined('ABSPATH')) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

if (!defined('PREHAFF_FIXING_ORDER_API_FILE')) {
	define('PREHAFF_FIXING_ORDER_API_FILE', __FILE__);
}

require_once(dirname(PREHAFF_FIXING_ORDER_API_FILE) . '/models/fixing_order.php');

register_activation_hook(__FILE__, 'prehaff_fixing_order_active');
register_deactivation_hook(__FILE__, 'prehaff_fixing_order_deactive');

function prehaff_fixing_order_inactive()
{
	delete_option('prehaff_fixing_order_db_version');
}

function prehaff_fixing_order_active()
{
	$fixing_order = new Fixing_Order();
	$fixing_order->createDB();
}

/*============== PREHAFF Register API fixing order ===================*/
add_action('rest_api_init', array('PREHAFF_Order_Fixing', 'prehaff_api_register_fixing_order_route'));

/*============== Add PREHAFF Order Form Shortcode ===================*/
add_shortcode('prehaff_order_form', array('PREHAFF_Order_Fixing', 'prehaff_order_form_shortcode'));

class PREHAFF_Order_Fixing
{
	private static $PAYMENT_METHODS = [
		1 => 'Thanh toán sau',
		2 => 'Chuyển khoản qua ngân hàng'
	];

	private static $ORDER_TIMES = [
		1 => 'Buổi sáng (8h30 - 12h)',
		2 => 'Buổi chiều (12h - 18h)',
		3 => 'Buổi tối (18h - 20h)'
	];

	// Variables
	public $fixingOrderModel;

	// Constructor
	public function __construct()
	{
		$this->fixingOrderModel = new Fixing_Order();

		/*============== Add PREHAFF Order Fixing To Admin ===================*/
		add_action('admin_menu', array($this, 'prehaff_fixing_order_admin_menu'));
	}

	//---> Functions

	/*============== PREHAFF Register API get fixing order ===================
	*
	*/
	public function prehaff_api_register_fixing_order_route()
	{
		register_rest_route('prehaff/v1', '/fixing-order', array(
			'methods' => WP_REST_Server::READABLE,
			'callback'	=>	array('PREHAFF_Order_Fixing', 'prehaff_api_get_fixing_order'),
		));
		register_rest_route('prehaff/v1', '/fixing-order/create', array(
			'methods' => WP_REST_Server::READABLE,
			'callback'	=>	array('PREHAFF_Order_Fixing', 'prehaff_api_create_fixing_order'),
		));
		register_rest_route('prehaff/v1', '/fixing-order/delete', array(
			'methods' => WP_REST_Server::READABLE,
			'callback'	=>	array('PREHAFF_Order_Fixing', 'prehaff_api_delete_fixing_order'),
		));
	}

	/*============== PREHAFF API excute request get fixing order ===================
	*
	*/
	public function prehaff_api_get_fixing_order($request)
	{
		$return = [
			'status'	=> false,
			'message'	=> 'Đã có lỗi xảy ra trong quá trình lấy dữ liệu đặt lịch, xin hãy f5 lại.'
		];

		$fixing_order = new Fixing_Order();
		$fixing_orders = $fixing_order->get();

		if (isset($fixing_orders)) {
			if (count($fixing_orders) > 0) {
				foreach ($fixing_orders as $key => $value) {
					$post = get_post($value->post_id);
					$value->post_title = $post->post_title;
					$value->payment_method = PREHAFF_Order_Fixing::$PAYMENT_METHODS[$value->payment_method];
					$value->order_time = PREHAFF_Order_Fixing::$ORDER_TIMES[$value->order_time];
				}
			}

			$return['message'] = 'Lấy dữ liệu thành công';
			$return['status'] = true;
			$return['fixing_orders'] = $fixing_orders;
		}

		return rest_ensure_response($return);
	}

	/*============== PREHAFF API excute request create fixing order ===================
	*
	*/
	public function prehaff_api_create_fixing_order($request)
	{
		$return = [
			'status'	=> false,
			'message'	=> 'Đã có lỗi xảy ra trong quá trình đặt lịch, xin hãy thử lại.'
		];

		$data = [
			'post_id' => (!empty($request['post_id'])) ? (int) $request['post_id'] : null,
			'customer_name' => (!empty($request['customer_name'])) ? (string) $request['customer_name'] : null,
			'customer_phone' => (!empty($request['customer_phone'])) ? (string) $request['customer_phone'] : null,
			'customer_email' => (!empty($request['customer_email'])) ? (string) $request['customer_email'] : null,
			'order_date' => (!empty($request['order_date'])) ? (string) $request['order_date'] : null,
			'order_time' => (!empty($request['order_time'])) ? (int) $request['order_time'] : null,
			'notes' => (!empty($request['notes'])) ? (string) $request['notes'] : null,
			'payment_method' => (!empty($request['payment_method'])) ? (int) $request['payment_method'] : null
		];

		$fixing_order = new Fixing_Order();
		$result = $fixing_order->add($data);

		if ($result) {
			$return['message'] = 'Đặt lịch thành công';
			$return['status'] = true;
			$post = get_post($data['post_id']);

			$to = get_option( 'admin_email' );
			$subject = 'Đặt lịch sửa chữa ngày:' . $data['order_date'] . ' vào lúc: ' . PREHAFF_Order_Fixing::$ORDER_TIMES[$data['order_time']];
			$message = '<p>Bạn có 1 đơn đặt lịch sửa chữa ngày: ' . $data['order_date'] . ' vào lúc: ' .  PREHAFF_Order_Fixing::$ORDER_TIMES[$data['order_time']] . '</p>';
			$message .= '<p>Tên dịch vụ: ' . $post->post_title . '</p>';
			$message .= '<p>Khách hàng tên: ' . $data['customer_name'] . '</p>';
			$message .= '<p>Khách hàng SDT: ' . $data['customer_phone'] . '</p>';
			$message .= '<p>Khách hàng Email: ' . $data['customer_email'] . '</p>';
			$message .= '<p>Khách hàng ghi chú: ' . $data['notes'] . '</p>';
			$message .= '<p>Phương thức thanh toán: ' . PREHAFF_Order_Fixing::$PAYMENT_METHODS[$data['payment_method']] . '</p>';
			add_filter('wp_mail_content_type', function ($content_type) {
				return 'text/html';
			});
			wp_mail($to, $subject, $message);
		}

		return rest_ensure_response($return);
	}

	/*============== PREHAFF API excute request delete fixing order ===================
	*
	*/
	public function prehaff_api_delete_fixing_order($request)
	{
		$id = (!empty($request['id'])) ? (int) $request['id'] : null;
		$return = [
			'status'	=> false,
			'message'	=> 'Đã có lỗi xảy ra trong quá trình xóa đặt lịch, xin hãy thử lại.'
		];

		if (!$id) {
			$return['message'] = 'Cần chọn đặt lịch muốn xóa';
			return $return;
		}

		$fixing_order = new Fixing_Order();
		$result = $fixing_order->delete($id);

		if ($result) {
			$return['message'] = 'Xóa đặt lịch thành công';
			$return['status'] = true;
		}

		return rest_ensure_response($return);
	}

	/*============== PREHAFF Order Fixing Admin Menu ===================*/
	public function prehaff_fixing_order_admin_menu()
	{
		add_menu_page(
			'PREHAFF Order Fixing',
			'PREHAFF Order Fixing',
			'manage_options',
			'prehaff-fixing-order',
			array($this, 'prehaff_fixing_order_index_page'),
			'dashicons-translation',
			30
		);
	}

	/*============== Content of page PREHAFF Order Fixing index ===================*/
	public function prehaff_fixing_order_index_page()
	{
		/*============== Add Bootstrap Model ===================*/
		wp_enqueue_style('modal', plugins_url('/assets/css/w3.css', __FILE__), array(), null, false);
		?>

		<!-- CSS -->
		<?php $this->print_css(); ?>

		<!-- JS -->
		<?php $this->print_js(); ?>

		<!-- HTML -->
		<div id="alert-message"></div>
		<div id="page-fixing-order" class="wrap">
			<h2>Lịch Đặt Sản Phẩm</h2>
			<!-- Modal Trigger -->
			<button id="open-modal-order" onclick="document.getElementById('modal-create-fixing-order').style.display='block'" class="w3-button w3-green">Đặt lịch</button>
			<!-- table -->
			<div class="row">
				<div class="col-md-12">
					<table class="wp-list-table widefat fixed striped">
						<thead>
							<tr>
								<th colspan="1">ID</th>
								<th colspan="3">Sản phẩm</th>
								<th colspan="3">Tên</th>
								<th colspan="3">Số điện thoại</th>
								<th colspan="3">Email</th>
								<th colspan="3">Ngày đặt</th>
								<th colspan="3">Thời gian đặt</th>
								<th colspan="3">Ghi chú</th>
								<th colspan="3">Phương thức thanh toán</th>
								<th colspan="3">Action</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="28" style="text-align: center;">Loading data</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<!-- Bootstrap Modal -->
			<!-- Modal Starts -->
			<div id="modal-create-fixing-order" class="w3-modal">
				<form class="w3-container w3-card-4">
					<div class="w3-modal-content">
						<header class="w3-container w3-teal">
							<span onclick="document.getElementById('modal-create-fixing-order').style.display='none'" class="w3-button w3-display-topright">&times;</span>
							<h2>Đặt lịch</h2>
						</header>
						<div class="w3-container">
							<p>
								<label class="w3-text-grey">Sản phẩm</label>
								<input class="w3-input w3-border" type="text" required="" name="post_id">
							</p>
							<p>
								<label class="w3-text-grey">Họ và tên</label>
								<input class="w3-input w3-border" type="text" name="customer_name">
							</p>
							<p>
								<label class="w3-text-grey">Số điện thoại</label>
								<input class="w3-input w3-border" type="text" required="" name="customer_phone">
							</p>
							<p>
								<label class="w3-text-grey">Email</label>
								<input class="w3-input w3-border" type="text" name="customer_email">
							</p>
							<p>
								<label class="w3-text-grey">Ngày</label>
								<input class="w3-input w3-border" type="text" required="" name="order_date">
							</p>
							<p>
								<label class="w3-text-grey">Thời gian</label>
								<select class="w3-select" name="order_time" required="">
									<option value="" disabled selected>Chọn thời gian</option>
									<option value="1"><?php echo PREHAFF_Order_Fixing::$ORDER_TIMES[1]; ?></option>
									<option value="2"><?php echo PREHAFF_Order_Fixing::$ORDER_TIMES[2]; ?></option>
									<option value="3"><?php echo PREHAFF_Order_Fixing::$ORDER_TIMES[3]; ?></option>
								</select>
							</p>
							<p>
								<label class="w3-text-grey">Ghi chú</label>
								<textarea class="w3-input w3-border" style="resize:none" name="notes"></textarea>
							</p>
							<p>
								<label class="w3-text-grey">Phương thức thanh toán</label>
								<select class="w3-select" name="payment_method" required="">
									<option value="" disabled selected>Chọn phương thức thanh toán</option>
									<option value="1"><?php echo PREHAFF_Order_Fixing::$PAYMENT_METHODS[1]; ?></option>
									<option value="2"><?php echo PREHAFF_Order_Fixing::$PAYMENT_METHODS[2]; ?></option>
								</select>
							</p>
						</div>
						<footer class="w3-container w3-teal">
							<p><button type="submit" class="w3-btn w3-white w3-border w3-border-blue w3-round" style="width:120px">Đặt lịch</button></p>
						</footer>
					</div>
				</form>
			</div>
			<!-- Modal Ends -->
		</div>
	<?php }

	/*============== Function print payment method text ===================*/
	public function printPaymentMethod($code)
	{
		return $this->payment_method[$code];
	}

	/*============== JS ===================*/
	function print_js()
	{
		?>
		<script type="text/javascript">
			var $ = jQuery;
			var urlSite = window.location.origin;
			var urlGetFixingOrders = urlSite + "/wp-json/prehaff/v1/fixing-order";
			var urlCreateFixingOrder = urlSite + "/wp-json/prehaff/v1/fixing-order/create";
			var urlDeleteFixingOrder = urlSite + "/wp-json/prehaff/v1/fixing-order/delete";

			// Get Fixing Orders
			function getFixingOrders() {
				var params = {
					// id: id,
				};
				$.get(urlGetFixingOrders, params).done(function(data) {
					if (data.status) {
						printFixingOrder(data.fixing_orders);
					} else {
						alert(data.message);
					}
				});
			}

			function printFixingOrder(data) {
				var html = '';
				if (data && data.length > 0) {
					data.forEach(function(val, idx) {
						html += '<tr id="row_key_' + val.id + '">\
																																																													<td colspan="1">' + val.id + '</td>\
																																																													<td colspan="3">' + val.post_title + '</td>\
																																																													<td colspan="3">' + val.customer_name + '</td>\
																																																													<td colspan="3">' + val.customer_phone + '</td>\
																																																													<td colspan="3">' + val.customer_email + '</td>\
																																																													<td colspan="3">' + val.order_date + '</td>\
																																																													<td colspan="3">' + val.order_time + '</td>\
																																																													<td colspan="3">' + val.notes + '</td>\
																																																													<td colspan="3">' + val.payment_method + '</td>\
																																																													<td colspan="3"><a href="#" class="w3-btn w3-white w3-border w3-border-red w3-text-red w3-round-large" onclick="deleteOrder(' + val.id + ')">Delete</a></td>\
																																																												</tr>';
					});
				} else {
					html = '<tr>\
																																																												<td colspan="28" style="text-align: center;">There is no data!!!</td>\
																																																											</tr>';
				}

				$('.wp-list-table tbody').html(html);
			}

			function showAlertMessage(status, el_class, message) {
				var html = '';
				html += '<div class="w3-panel ' + el_class + ' w3-display-container w3-border">\
																					<span onclick="hideAlertMessage();" class="w3-button w3-display-topright">&times;</span>\
																					<h3>' + status + '</h3>\
																					<p>' + message + '</p>\
																				</div>';
				$('#alert-message').html(html);
				$('#alert-message').show();
			}

			function hideAlertMessage() {
				var html = '';
				$('#alert-message').html(html);
				$('#alert-message').hide();
			}

			function deleteOrder(id) {
				var params = {
					id: id,
				};
				$.get(urlDeleteFixingOrder, params).done(function(data) {
					if (data.status) {
						showAlertMessage('Thành Công !!!', 'w3-pale-green', data.message);
						getFixingOrders();
					} else {
						showAlertMessage('Không Thành Công !!!', 'w3-pale-red', data.message);
					}
				});
			}

			$(document).ready(function() {
				getFixingOrders();

				$('#modal-create-fixing-order form').on("submit", function(event) {
					document.getElementById('modal-create-fixing-order').style.display = 'none';
					event.preventDefault();

					var params = $(this).serialize();
					$.get(urlCreateFixingOrder, params).done(function(data) {
						if (data.status) {
							showAlertMessage('Thành Công !!!', 'w3-pale-green', data.message);
							getFixingOrders();
						} else {
							showAlertMessage('Không Thành Công !!!', 'w3-pale-red', data.message);
						}
					});
				});
			});
		</script>
	<?php
	}

	/*============== CSS ===================*/
	function print_css()
	{ ?>
		<style type="text/css">
			#page-fixing-order {
				position: relative;
			}

			#page-fixing-order #open-modal-order {
				position: absolute;
				right: 10px;
				top: 5px;
			}

			#page-fixing-order>h2 {
				margin-bottom: 10px;
			}
		</style>
	<?php }

	/*============== Function define shortcode for show order form ===================*/
	public function prehaff_order_form_shortcode($atts, $content = null)
	{
		global $post, $product;

		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));

		ob_start();
		// echo js script
		echo '<script type="text/javascript">
		var $ = jQuery;
		var urlSite = window.location.origin;
		var urlCreateFixingOrder = urlSite + "/wp-json/prehaff/v1/fixing-order/create";

		$(document).ready(function() {
			$(\'#loading-spinner\').hide();
			$("#banking-info").hide();

			// Get table html
			var priceHTML = "";
			if($(".col-sm-9 table").length > 0) {
				priceHTML = $(".col-sm-9 table").prop("outerHTML");
			} else {
				priceHTML = "<h3>"+$(".info-wrapper .woocommerce-Price-amount").text()+"</h3>";
			}
			$("#modal-create-fixing-order .product-info .price-wrapper").html(priceHTML);

			// Submit order form
			$(\'#modal-create-fixing-order form\').on("submit", function(event) {
				$(\'#btn-submit\').hide();
				$(\'#loading-spinner\').show();
				event.preventDefault();

				var payment_method = $(\'select[name="payment_method"]\').val();
				var params = $(this).serialize();
				$.get(urlCreateFixingOrder, params).done(function(data) {
					$(\'#loading-spinner\').hide();
					$(\'#btn-submit\').show();
					if (data.status) {
						$(\'#modal-create-fixing-order\').hide();
						if(payment_method == "2") {
							$("#banking-info").show();
						}
						$(\'#modal-order-success\').show();
					}
				});
			});
		});
	</script>';
		// <span class="sub-title">' . $post->post_title . '</span>
		// echo form order
		echo '<div class="order-form ' . $el_class . '">
			<div id="modal-create-fixing-order" class="w3-modal">
				<form class="w3-container w3-card-4">
					<input type="hidden" name="post_id" value="' . $post->ID . '">
					<div class="w3-modal-content">
						<header class="w3-container  w3-red">
							<span onclick="document.getElementById(\'modal-create-fixing-order\').style.display=\'none\'" class="w3-button w3-display-topright">&times;</span>
							<h2><span class="main-title">Đặt lịch hẹn cho dịch vụ</span> </h2>
						</header>
						<div class="w3-container">
							<div class="w3-row-padding">
								<div class="w3-col product-info">
									<div class="basic">
										<div class="image-wrapper">
											<img src="' . get_the_post_thumbnail_url($post) . '">
										</div>
										<div class="info-wrapper">
											<h3>' . $post->post_title . '</h3>
										</div>
									</div>
									<div class="col-12 price-wrapper"></div>
								</div>
							</div>
							<div class="w3-row-padding customer-info">
								<h4>Thông tin của quý khách</h4>
								<div class="w3-half">
									<p>
										<label class="w3-text-grey">Họ và tên</label>
										<input class="w3-input w3-border" type="text" name="customer_name">
									</p>
								</div>
								<div class="w3-half">
									<p>
										<label class="w3-text-grey">Số điện thoại</label>
										<input class="w3-input w3-border" type="number" required="" name="customer_phone">
									</p>
								</div>
								<div class="w3-half">
									<p>
										<label class="w3-text-grey">Email</label>
										<input class="w3-input w3-border" type="text" name="customer_email">
									</p>
								</div>
								<div class="w3-half">
									<p>
										<label class="w3-text-grey">Ngày</label>
										<input class="w3-input w3-border" type="date" required="" name="order_date">
									</p>
								</div>
								<div class="w3-half">
									<p>
										<label class="w3-text-grey">Thời gian</label>
										<select class="w3-select" name="order_time" required="">
											<option value="" disabled selected>Chọn thời gian</option>
											<option value="1">' . PREHAFF_Order_Fixing::$ORDER_TIMES[1] . '</option>
											<option value="2">' . PREHAFF_Order_Fixing::$ORDER_TIMES[2] . '</option>
											<option value="3">' . PREHAFF_Order_Fixing::$ORDER_TIMES[3] . '</option>
										</select>
									</p>
								</div>
								<div class="w3-half">
									<p>
										<label class="w3-text-grey">Phương thức thanh toán</label>
										<select class="w3-select" name="payment_method" required="">
											<option value="" disabled selected>Chọn phương thức thanh toán</option>
											<option value="1">' . PREHAFF_Order_Fixing::$PAYMENT_METHODS[1] . '</option>
											<option value="2">' . PREHAFF_Order_Fixing::$PAYMENT_METHODS[2] . '</option>
										</select>
									</p>
								</div>
								<div class="w3-col">
									<p>
										<label class="w3-text-grey">Ghi chú</label>
										<textarea class="w3-input w3-border" style="resize:none" name="notes"></textarea>
									</p>
								</div>
							</div>
						</div>
						<footer class="w3-container  w3-red">
							<p id="btn-submit"><button type="submit" class="w3-btn w3-white w3-border w3-border-blue w3-round" style="width:120px">Đặt lịch</button></p>
							<p id="loading-spinner"><i class="fa fa-spinner w3-spin" style="font-size:64px"></i></p>
						</footer>
					</div>
				</form>
			</div>
		</div>';

		// echo modal success
		echo '<div class="order-form ' . $el_class . '">
			<div id="modal-order-success" class="w3-modal">
				<div class="w3-modal-content">
					<header class="w3-container w3-teal">
						<span onclick="document.getElementById(\'modal-order-success\').style.display=\'none\'" class="w3-button w3-display-topright">&times;</span>
						<h2>Đặt lịch hẹn thành công</h2>
					</header>
					<div class="w3-container">
						<div class="w3-row-padding">
							<div class="w3-col">
								<p>Bạn đã đặt thành công lịch hẹn cho dịch vụ ' . $post->post_title . ' </p>
								<div id="banking-info">
									<p><strong>Thông tin chuyển khoản:</strong></p>
									<p>Chủ tài khoản: <strong>'.get_option( 'prehaff_bank_owner' ).'</strong></p>
									<p>Số tài khoản: <strong>'.get_option( 'prehaff_bank_number' ).'</strong></p>
								</div>
							</div>
						</div>
					</div>
					<footer class="w3-container w3-teal">
						<p><button type="button" class="w3-btn w3-white w3-border w3-border-blue w3-round" style="width:120px" onclick="document.getElementById(\'modal-order-success\').style.display=\'none\'">Đóng</button></p>
					</footer>
				</div>
			</div>
		</div>';
		return ob_get_clean();
	}
}


if (is_admin()) {
	new PREHAFF_Order_Fixing();
}
