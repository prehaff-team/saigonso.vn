<?php

global $prehaff_fixing_order_db_version;
$prehaff_fixing_order_db_version = '1.0';

class Fixing_Order
{
	const FIXING_ORDER_DB = 'fixing_orders';

	public function __construct()
	{ }

	public function createDB()
	{
		global $wpdb;
		global $prehaff_fixing_order_db_version;

		add_option('prehaff_fixing_order_db_version', $prehaff_fixing_order_db_version);

		$table_name = $wpdb->prefix . self::FIXING_ORDER_DB;

		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE $table_name (
			id int(11) unsigned NOT NULL AUTO_INCREMENT,
			post_id int NOT NULL,
			customer_name tinytext,
			customer_phone tinytext NOT NULL,
			customer_email tinytext,
			order_date date NOT NULL,
			order_time int(11) NOT NULL,
			notes tinytext,
			status  int,
			created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  			payment_method int(11) NOT NULL,
		) $charset_collate;";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);
	}

	public function add($data)
	{
		global $wpdb;
		$result = $wpdb->insert(
			$wpdb->prefix . self::FIXING_ORDER_DB,
			array(
				'post_id' => $data['post_id'],
				'customer_name' => $data['customer_name'],
				'customer_phone' => $data['customer_phone'],
				'customer_email' => $data['customer_email'],
				'order_date' => $data['order_date'],
				'order_time' => $data['order_time'],
				'notes' => $data['notes'],
				'status' => $data['status'],
				'payment_method' => $data['payment_method'],
			)
		);

		if($result) {
			return $wpdb->insert_id;
		}

		return false;
	}

	public function get()
	{
		global $wpdb;
		$table_name = $wpdb->prefix . self::FIXING_ORDER_DB;
		$rows = $wpdb->get_results("select * from " . $table_name . " order by created_at DESC;");
		return $rows;
	}

	public function delete($id)
	{
		global $wpdb;
		$table_name = $wpdb->prefix . self::FIXING_ORDER_DB;
		// Then delete record have id = id
		return $wpdb->delete($table_name, array('id' => $id), array('%d'));
	}
}
