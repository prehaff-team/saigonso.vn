<?php
/*
* Plugin Name: WooCommerce Single Product Page Builder
* Plugin URI: http://sitesao.com/
* Description: Woocommerce single product page builder for Visual Composer
* Version: 2.0.2
* Author: SiteSao Team
* Author URI: http://sitesao.com/
* License: License GNU General Public License version 2 or later;
* Copyright 2013  SiteSao
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if(!defined('DHVC_WOO_PAGE'))
	define('DHVC_WOO_PAGE','dhvc-woocommerce-page');

if(!defined('DHVC_WOO_PAGE_VERSION'))
	define('DHVC_WOO_PAGE_VERSION','2.0.2');

if(!defined('DHVC_WOO_PAGE_URL'))
	define('DHVC_WOO_PAGE_URL',untrailingslashit( plugins_url( '/', __FILE__ ) ));

if(!defined('DHVC_WOO_PAGE_DIR'))
	define('DHVC_WOO_PAGE_DIR',untrailingslashit( plugin_dir_path( __FILE__ ) ));

if (!function_exists('dhwc_is_active')){
	/**
	 * Check woocommerce plugin is active
	 *
	 * @return boolean .TRUE is active
	 */
	function dhwc_is_active(){
		
		$active_plugins = (array) get_option( 'active_plugins', array() );

		if ( is_multisite() )
			$active_plugins = array_merge($active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );

		return in_array( 'woocommerce/woocommerce.php', $active_plugins ) || array_key_exists( 'woocommerce/woocommerce.php', $active_plugins );
	}
}

if(!class_exists('DHVC_Woo_Page')):

global $dhvc_single_product_template_id,$dhvc_single_product_template;

class DHVC_Woo_Page{
	
	protected $_shortcode_class;
	
	public function __construct(){
		add_action('init',array(&$this,'init'));
	}
	
	public function init(){
		load_plugin_textdomain( DHVC_WOO_PAGE, false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
		wp_register_style('dhvc-woo-page-chosen', DHVC_WOO_PAGE_URL.'/assets/css/chosen.min.css');
		
		if ( ! defined( 'WPB_VC_VERSION' ) ) {
			add_action('admin_notices', array(&$this,'notice'));
			return;
		}
		
		if(!dhwc_is_active()){
			add_action('admin_notices', array(&$this,'woocommerce_notice'));
			return;
		}
		
		require_once DHVC_WOO_PAGE_DIR.'/includes/functions.php';
		require_once DHVC_WOO_PAGE_DIR.'/includes/map.php';
		
		if(!is_admin()){
			require_once DHVC_WOO_PAGE_DIR.'/includes/shortcode.php';
			$this->_shortcode_class = new DHVC_Woo_Page_Shortcode();
		}
		$params_script = DHVC_WOO_PAGE_URL.'/assets/js/params.js';
		vc_add_shortcode_param ( 'dhvc_woo_product_page_field_categories', 'dhvc_woo_product_page_setting_field_categories',$params_script);
		vc_add_shortcode_param ( 'dhvc_woo_product_page_field_products_ajax', 'dhvc_woo_product_page_setting_field_products_ajax',$params_script);
		
		
		if(is_admin()):
			require_once DHVC_WOO_PAGE_DIR.'/includes/admin.php';
		else:
			add_action( 'template_redirect', array( &$this, 'get_register_single_product_template' ) );
			add_action( 'template_redirect', array( &$this, 'register_assets' ) );
			add_action(	'wp_enqueue_scripts', array(&$this, 'frontend_assets'));
			add_filter(	'wc_get_template_part', array(&$this,'wc_get_template_part'),50,3);
			
			if(apply_filters('dhvc_woocommerce_page_use_custom_single_product_template',false))
				add_filter( 'template_include', array( &$this, 'template_loader' ),50 );
		endif;

	}
	
	public function register_assets(){
		wp_register_style('dhvc-woocommerce-page-awesome', DHVC_WOO_PAGE_URL.'/assets/fonts/awesome/css/font-awesome.min.css',array(),'4.0.3');
		wp_register_style('dhvc-woocommerce-page', DHVC_WOO_PAGE_URL.'/assets/css/style.css',array(),DHVC_WOO_PAGE_VERSION);
	}
	
	public function frontend_assets(){
		wp_enqueue_style('js_composer_front');
		wp_enqueue_style('js_composer_custom_css');
		wp_enqueue_style('dhvc-woocommerce-page-awesome');
		wp_enqueue_style('dhvc-woocommerce-page');
	}
	
	protected function has_wc_single_product_shortcode(){
		global $shortcode_tags;	
		foreach ($this->_shortcode_class->get_shortcodes() as $shortcode=>$fnc)
			if(array_key_exists( $shortcode, $shortcode_tags ))
				return true;
		
		return false;
	}
	
	public function get_register_single_product_template(){
		global $post,$dhvc_single_product_template_id;
		
		if(empty($post))
			return;
		
		$product_template_id = apply_filters('dhvc_woocommerce_page_default_template_id', 0);
		
		if($dhvc_woo_page_product = get_post_meta($post->ID,'dhvc_woo_page_product',true)):
			$product_template_id = $dhvc_woo_page_product;
		else:
			$terms = wp_get_post_terms( $post->ID, 'product_cat' );
			foreach ( $terms as $term ):
				if($dhvc_woo_page_cat_product = get_woocommerce_term_meta($term->term_id,'dhvc_woo_page_cat_product',true)):
					$product_template_id = $dhvc_woo_page_cat_product;
				endif;
			endforeach;
		endif;
		
		if(!empty($product_template_id)){
			$dhvc_single_product_template_id = $product_template_id;
		}
		
		
		do_action('dhvc_woocommerce_page_register_single',$product_template_id);
		
		return $product_template_id;
	}
	
	public function template_loader( $template ) {
		global $dhvc_single_product_template_id;
		if ( is_singular('product')) {
			$find = array();
			if(empty($dhvc_single_product_template_id))
				$dhvc_single_product_template_id = $this->get_register_single_product_template();
			$file 	= 'single-product.php';
			$find[] = 'dhvc-woocommerce-page/'.$file;
			if($dhvc_single_product_template = get_post($dhvc_single_product_template_id)){
				$template       = locate_template( $find );
				$status_options = get_option( 'woocommerce_status_options', array() );
				if ( ! $template || ( ! empty( $status_options['template_debug_mode'] ) && current_user_can( 'manage_options' ) ) )
					$template = $this->get_plugin_dir() . '/templates/' . $file;
					
				return $template;
			}
		}
		return $template;
	}
	
	public function wc_get_template_part($template, $slug, $name){
		global $post,$dhvc_single_product_template,$dhvc_single_product_template_id;
		if(empty($dhvc_single_product_template_id))
			$dhvc_single_product_template_id = $this->get_register_single_product_template();
		if($slug === 'content' && $name === apply_filters('dhvc_woocommerce_page_single_product_temp_name', 'single-product')){
			do_action('dhvc_woocommerce_page_before_override');
			
			$file 	= 'content-single-product.php';
			$find[] = 'dhvc-woocommerce-page/' . $file;
			if(!empty($dhvc_single_product_template_id)){
				if($wpb_custom_css = get_post_meta( $dhvc_single_product_template_id, '_wpb_post_custom_css', true )){
					echo '<style type="text/css">'.$wpb_custom_css.'</style>';
				}
				if($wpb_shortcodes_custom_css = get_post_meta( $dhvc_single_product_template_id, '_wpb_shortcodes_custom_css', true )){
					echo '<style type="text/css">'.$wpb_shortcodes_custom_css.'</style>';
				}
				$dhvc_single_product_template = get_post($dhvc_single_product_template_id);
				if(class_exists('Ultimate_VC_Addons')){
					$backup_post = $post;
					$post  = $dhvc_single_product_template;
					$Ultimate_VC_Addons = new Ultimate_VC_Addons;
					$Ultimate_VC_Addons->aio_front_scripts();
					$post = $backup_post;
				}
				if($dhvc_single_product_template){
					$template       = locate_template( $find );
					if ( ! $template || ( ! empty( $status_options['template_debug_mode'] ) && current_user_can( 'manage_options' ) ) )
						$template = $this->get_plugin_dir() . '/templates/' . $file;
						
					return $template;
				}
			}
			do_action('dhvc_woocommerce_page_after_override');
		}
		return $template;
	}
	
	public function notice(){
		$plugin = get_plugin_data(__FILE__);
		echo '
			  <div class="updated">
			    <p>' . sprintf(__('<strong>%s</strong> requires <strong><a href="http://bit.ly/1gKaeh5" target="_blank">Visual Composer</a></strong> plugin to be installed and activated on your site.', DHVC_WOO), $plugin['Name']) . '</p>
			  </div>';
	}
	
	public function woocommerce_notice(){
		$plugin = get_plugin_data(__FILE__);
		echo '
			  <div class="updated">
			    <p>' . sprintf(__('<strong>%s</strong> requires <strong><a href="http://www.woothemes.com/woocommerce/" target="_blank">WooCommerce</a></strong> plugin to be installed and activated on your site.', DHVC_WOO), $plugin['Name']) . '</p>
			  </div>';
	}
	
	public function get_plugin_url(){
		return DHVC_WOO_PAGE_URL;
	}
	
	public function get_plugin_dir(){
		return DHVC_WOO_PAGE_DIR;
	}
	
}

new DHVC_Woo_Page();

endif;