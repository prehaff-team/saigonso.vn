<?php

/**
 * Plugin Name: PREHAFF Store
 * Plugin URI: https://bitbucket.org/prehaff-team/
 * Description: This is a plugin for management order product.
 * Version: 1.0 
 * Author: PREHAFF Team
 * Author URI: 
 * License: GPLv2 or later 
 */
?>
<?php

// Make sure we don't expose any info if called directly
if (!defined('ABSPATH')) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

if (!defined('PREHAFF_STORE_API_FILE')) {
	define('PREHAFF_STORE_API_FILE', __FILE__);
}

require_once(dirname(PREHAFF_STORE_API_FILE) . '/models/store.php');

register_activation_hook(__FILE__, 'prehaff_store_active');
register_deactivation_hook(__FILE__, 'prehaff_store_deactive');

function prehaff_store_deactive()
{
	delete_option('prehaff_store_db_version');
}

function prehaff_store_active()
{
	$store = new Store();
	$store->createDB();
}

/*============== PREHAFF Register API fixing order ===================*/
add_action('rest_api_init', array('PREHAFF_Store', 'prehaff_api_register_store_route'));

/*============== Add PREHAFF Order Form Shortcode ===================*/
add_shortcode('prehaff_sidebar_stores', array('PREHAFF_Store', 'prehaff_sidebar_stores_shortcode'));
add_shortcode('prehaff_footer_stores', array('PREHAFF_Store', 'prehaff_footer_stores_shortcode'));

class PREHAFF_Store
{
	// Variables
	public $storeModel;

	// Constructor
	public function __construct()
	{
		$this->storeModel = new Store();

		/*============== Add PREHAFF Store To Admin ===================*/
		add_action('admin_menu', array($this, 'prehaff_store_admin_menu'));

		add_action('admin_init', array($this, 'register_fields'));
	}

	//---> Functions

	/*============== PREHAFF Register API get fixing order ===================
	*
	*/
	public function prehaff_api_register_store_route()
	{
		register_rest_route('prehaff/v1', '/stores', array(
			'methods' => WP_REST_Server::READABLE,
			'callback'	=>	array('PREHAFF_Store', 'prehaff_api_get_store'),
		));
		register_rest_route('prehaff/v1', '/stores/create', array(
			'methods' => WP_REST_Server::READABLE,
			'callback'	=>	array('PREHAFF_Store', 'prehaff_api_create_store'),
		));
		register_rest_route('prehaff/v1', '/stores/delete', array(
			'methods' => WP_REST_Server::READABLE,
			'callback'	=>	array('PREHAFF_Store', 'prehaff_api_delete_store'),
		));
	}

	/*============== PREHAFF API excute request get fixing order ===================
	*
	*/
	public function prehaff_api_get_store($request)
	{
		$return = [
			'status'	=> false,
			'message'	=> 'Đã có lỗi xảy ra trong quá trình lấy dữ liệu chi nhánh, xin hãy f5 lại.'
		];

		$store = new Store();
		$stores = $store->get();

		if (isset($stores)) {
			$return['message'] = 'Lấy dữ liệu thành công';
			$return['status'] = true;
			$return['stores'] = $stores;
		}

		return rest_ensure_response($return);
	}

	/*============== PREHAFF API excute request create fixing order ===================
	*
	*/
	public function prehaff_api_create_store($request)
	{
		$return = [
			'status'	=> false,
			'message'	=> 'Đã có lỗi xảy ra trong quá trình chi nhánh, xin hãy thử lại.'
		];

		$data = [
			'name' => (!empty($request['name'])) ? (string) $request['name'] : null,
			'phone' => (!empty($request['phone'])) ? (string) $request['phone'] : null,
			'phone_text' => (!empty($request['phone_text'])) ? (string) $request['phone_text'] : null,
			// 'map_id' => (!empty($request['map_id'])) ? (int) $request['map_id'] : null,
			'map_url' => (!empty($request['map_url'])) ? (int) $request['map_url'] : null
		];

		$store = new Store();
		$result = $store->add($data);

		if ($result) {
			$return['message'] = 'Tạo chi nhánh thành công';
			$return['status'] = true;
		}

		return rest_ensure_response($return);
	}

	/*============== PREHAFF API excute request delete fixing order ===================
	*
	*/
	public function prehaff_api_delete_store($request)
	{
		$id = (!empty($request['id'])) ? (int) $request['id'] : null;
		$return = [
			'status'	=> false,
			'message'	=> 'Đã có lỗi xảy ra trong quá trình xóa chi nhánh, xin hãy thử lại.'
		];

		if (!$id) {
			$return['message'] = 'Cần chọn chi nhánh muốn xóa';
			return $return;
		}

		$store = new Store();
		$result = $store->delete($id);

		if ($result) {
			$return['message'] = 'Xóa chi nhánh thành công';
			$return['status'] = true;
		}

		return rest_ensure_response($return);
	}

	/*============== PREHAFF Store Admin Menu ===================*/
	public function prehaff_store_admin_menu()
	{
		add_menu_page(
			'PREHAFF Store',
			'PREHAFF Store',
			'manage_options',
			'prehaff-stores',
			array($this, 'prehaff_store_index_page'),
			'dashicons-translation',
			30
		);
	}

	/*============== Content of page PREHAFF Store index ===================*/
	public function prehaff_store_index_page()
	{
		/*============== Add Bootstrap Model ===================*/
		wp_enqueue_style('modal', plugins_url('/assets/css/w3.css', __FILE__), array(), null, false);
		?>

		<!-- CSS -->
		<?php $this->print_css(); ?>

		<!-- JS -->
		<?php $this->print_js(); ?>

		<!-- HTML -->
		<div id="alert-message"></div>
		<div id="page-stores" class="wrap">
			<h2>Danh sách chi nhánh</h2>
			<!-- Modal Trigger -->
			<button id="open-modal-order" onclick="document.getElementById('modal-create-stores').style.display='block'" class="w3-button w3-green">Tạo chi nhánh</button>
			<!-- table -->
			<div class="row">
				<div class="col-md-12">
					<table class="wp-list-table widefat fixed striped">
						<thead>
							<tr>
								<th colspan="1">ID</th>
								<th colspan="3">Địa chỉ</th>
								<th colspan="3">Số điện thoại</th>
								<th colspan="3">Số điện thoại (text)</th>
								<!-- <th colspan="3">ID Google Map</th> -->
								<th colspan="3">Link Google Map</th>
								<th colspan="3">Action</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="16" style="text-align: center;">Loading data</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<!-- Bootstrap Modal -->
			<!-- Modal Starts -->
			<div id="modal-create-stores" class="w3-modal">
				<form class="w3-container w3-card-4">
					<div class="w3-modal-content">
						<header class="w3-container w3-teal">
							<span onclick="document.getElementById('modal-create-stores').style.display='none'" class="w3-button w3-display-topright">&times;</span>
							<h2>Tạo chi nhánh</h2>
						</header>
						<div class="w3-container">
							<p>
								<label class="w3-text-grey">Địa chỉ</label>
								<input class="w3-input w3-border" type="text" name="name">
							</p>
							<p>
								<label class="w3-text-grey">Số điện thoại</label>
								<input class="w3-input w3-border" type="text" required="" name="phone">
							</p>
							<p>
								<label class="w3-text-grey">Số điện thoại (text)</label>
								<input class="w3-input w3-border" type="text" name="phone_text">
							</p>
							<!-- <p>
								<label class="w3-text-grey">ID Google Map</label>
								<input class="w3-input w3-border" type="text" required="" name="map_id">
							</p> -->
							<p>
								<label class="w3-text-grey">Link Google Map</label>
								<input class="w3-input w3-border" type="text" required="" name="map_url">
							</p>
						</div>
						<footer class="w3-container w3-teal">
							<p><button type="submit" class="w3-btn w3-white w3-border w3-border-blue w3-round" style="width:120px">Tạo chi nhánh</button></p>
						</footer>
					</div>
				</form>
			</div>
			<!-- Modal Ends -->
		</div>
	<?php }

	/*============== JS ===================*/
	function print_js()
	{
		?>
		<script type="text/javascript">
			var $ = jQuery;
			var urlSite = window.location.origin;
			var urlGetStores = urlSite + "/wp-json/prehaff/v1/stores";
			var urlCreateStore = urlSite + "/wp-json/prehaff/v1/stores/create";
			var urlDeleteStore = urlSite + "/wp-json/prehaff/v1/stores/delete";

			// Get Fixing Orders
			function getStores() {
				var params = {
					// id: id,
				};
				$.get(urlGetStores, params).done(function(data) {
					if (data.status) {
						printStore(data.stores);
					} else {
						alert(data.message);
					}
				});
			}

			function printStore(data) {
				var html = '';
				if (data && data.length > 0) {
					data.forEach(function(val, idx) {
						html += '<tr id="row_key_' + val.id + '">\
																																																															<td colspan="1">' + val.id + '</td>\
																																																															<td colspan="3">' + val.name + '</td>\
																																																															<td colspan="3">' + val.phone + '</td>\
																																																															<td colspan="3">' + val.phone_text + '</td>\
																																																															<td colspan="3">' + val.map_url + '</td>\
																																																															<td colspan="3"><a href="#" class="w3-btn w3-white w3-border w3-border-red w3-text-red w3-round-large" onclick="deleteStore(' + val.id + ')">Delete</a></td>\
																																																														</tr>';
					});
				} else {
					html = '<tr>\
																																																														<td colspan="28" style="text-align: center;">There is no data!!!</td>\
																																																													</tr>';
				}

				$('.wp-list-table tbody').html(html);
			}

			function showAlertMessage(status, el_class, message) {
				var html = '';
				html += '<div class="w3-panel ' + el_class + ' w3-display-container w3-border">\
																							<span onclick="hideAlertMessage();" class="w3-button w3-display-topright">&times;</span>\
																							<h3>' + status + '</h3>\
																							<p>' + message + '</p>\
																						</div>';
				$('#alert-message').html(html);
				$('#alert-message').show();
			}

			function hideAlertMessage() {
				var html = '';
				$('#alert-message').html(html);
				$('#alert-message').hide();
			}

			function deleteStore(id) {
				var params = {
					id: id,
				};
				$.get(urlDeleteStore, params).done(function(data) {
					if (data.status) {
						showAlertMessage('Thành Công !!!', 'w3-pale-green', data.message);
						getStores();
					} else {
						showAlertMessage('Không Thành Công !!!', 'w3-pale-red', data.message);
					}
				});
			}

			$(document).ready(function() {
				getStores();

				$('#modal-create-stores form').on("submit", function(event) {
					document.getElementById('modal-create-stores').style.display = 'none';
					event.preventDefault();

					var params = $(this).serialize();
					$.get(urlCreateStore, params).done(function(data) {
						if (data.status) {
							showAlertMessage('Thành Công !!!', 'w3-pale-green', data.message);
							getStores();
						} else {
							showAlertMessage('Không Thành Công !!!', 'w3-pale-red', data.message);
						}
					});
				});
			});
		</script>
	<?php
	}

	/*============== CSS ===================*/
	function print_css()
	{ ?>
		<style type="text/css">
			#page-stores {
				position: relative;
			}

			#page-stores #open-modal-order {
				position: absolute;
				right: 10px;
				top: 5px;
			}

			#page-stores>h2 {
				margin-bottom: 10px;
			}
		</style>
	<?php }



	public function prehaff_sidebar_stores_shortcode($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));

		$store = new Store();
		$stores = $store->get();

		ob_start();
		echo '<div class="sidebar_stores ' . $el_class . '">';
		echo '<h4>Hệ thống cửa hàng</h4>';

		foreach ($stores as $store) {
			echo '<div class="detail-address-item">';
			echo '<p class="name">' . $store->name . '</p>';
			echo '<p class="hotline"><a href="tel:' . $store->phone . '">' . $store->phone_text . '</a> <a href="'.$store->map_url.'" target="_blank" class="google-map-modal"><i class="fa fa-location-arrow" aria-hidden="true"></i> Chỉ đường</a></p>';
			echo '</div>';
		}

		echo '</div>';
		return ob_get_clean();
	}

	public function prehaff_footer_stores_shortcode($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));

		$store = new Store();
		$stores = $store->get();

		ob_start();
		$i = 1;
		foreach ($stores as $store) {
			echo '<li class="item">';
			echo '<p>CN ' . $i . ': ' . $store->name . '</p>';
			echo '<p class="hotline"><a href="tel:' . $store->phone . '">' . $store->phone_text . '</a> <a href="' . $store->map_url . '" target="_blank" class="google-map-modal"><i class="fa fa-location-arrow" aria-hidden="true"></i> Chỉ đường</a></p>';
			echo '</li>';
			$i++;
		}

		return ob_get_clean();
	}
}


if (is_admin()) {
	new PREHAFF_Store();
}
