<?php

global $prehaff_store_db_version;
$prehaff_store_db_version = '1.0';

class Store
{
	const STORE_DB = 'prehaff_stores';

	public function __construct()
	{ }

	public function createDB()
	{
		global $wpdb;
		global $prehaff_store_db_version;

		add_option('prehaff_store_db_version', $prehaff_store_db_version);

		$table_name = $wpdb->prefix . self::STORE_DB;

		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE $table_name (
			id int(11) unsigned NOT NULL AUTO_INCREMENT,
			name text,
			phone text,
			phone_text text,
			map_url text,
			created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
			updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  			PRIMARY KEY (`id`)
		) $charset_collate;";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);
	}

	public function add($data)
	{
		global $wpdb;
		$result = $wpdb->insert(
			$wpdb->prefix . self::STORE_DB,
			array(
				'name' => $data['name'],
				'phone' => $data['phone'],
				'phone_text' => $data['phone_text'],
				'map_url' => $data['map_url']
			)
		);

		if ($result) {
			return $wpdb->insert_id;
		}

		return false;
	}

	public function get()
	{
		global $wpdb;
		$table_name = $wpdb->prefix . self::STORE_DB;
		$rows = $wpdb->get_results("select * from " . $table_name . " order by id ASC;");
		return $rows;
	}

	public function delete($id)
	{
		global $wpdb;
		$table_name = $wpdb->prefix . self::STORE_DB;
		// Then delete record have id = id
		return $wpdb->delete($table_name, array('id' => $id), array('%d'));
	}
}
