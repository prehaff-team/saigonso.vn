<?php

/**
 * Plugin Name: PREHAFF Utilities
 * Plugin URI: https://bitbucket.org/prehaff-team/
 * Description: This is a plugin to provide utilities made by PREHAFF Team.
 * Version: 1.0 
 * Author: PREHAFF Team
 * Author URI: 
 * License: GPLv2 or later 
 */
?>
<?php

// Make sure we don't expose any info if called directly
if (!defined('ABSPATH')) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

if (!defined('PREHAFF_UTILITIES_API_FILE')) {
	define('PREHAFF_UTILITIES_API_FILE', __FILE__);
}

register_activation_hook(__FILE__, 'prehaff_utilities_active');
register_deactivation_hook(__FILE__, 'prehaff_utilities_deactive');

function prehaff_utilities_inactive()
{
	delete_option('prehaff_utilities_db_version');
}

function prehaff_utilities_active()
{ }

class PREHAFF_Utilities
{
	// Variables

	// Constructor
	public function __construct()
	{
		// Add new fields to admin settings
		add_action('admin_init', array($this, 'register_fields'));
	}

	//---> Functions
	/**
	 * Add new fields to wp-admin/options-general.php page
	 */
	public function register_fields()
	{
		register_setting('general', 'prehaff_bank_owner', 'esc_attr');
		add_settings_field(
			'prehaff_bank_owner_id',
			'<label for="prehaff_bank_owner_id">' . __('Bank Owner', 'prehaff_bank_owner') . '</label>',
			array($this, 'prehaff_bank_owner_html'),
			'general'
		);


		register_setting('general', 'prehaff_bank_number', 'esc_attr');
		add_settings_field(
			'prehaff_bank_number_id',
			'<label for="prehaff_bank_number_id">' . __('Bank Number', 'prehaff_bank_number') . '</label>',
			array($this, 'prehaff_bank_number_html'),
			'general'
		);

		register_setting('general', 'prehaff_link_chat_facebook', 'esc_attr');
		add_settings_field(
			'prehaff_link_chat_facebook_id',
			'<label for="prehaff_link_chat_facebook_id">' . __('Link chat facebook', 'prehaff_link_chat_facebook') . '</label>',
			array($this, 'prehaff_link_chat_facebook_html'),
			'general'
		);

		register_setting('general', 'prehaff_link_chat_zalo', 'esc_attr');
		add_settings_field(
			'prehaff_link_chat_zalo_id',
			'<label for="prehaff_link_chat_zalo_id">' . __('Link chat zalo', 'prehaff_link_chat_zalo') . '</label>',
			array($this, 'prehaff_link_chat_zalo_html'),
			'general'
		);
	}

	/**
	 * HTML for extra settings
	 */
	public function prehaff_bank_owner_html()
	{
		$value = get_option('prehaff_bank_owner', '');
		echo '<input type="text" id="prehaff_bank_owner_id" name="prehaff_bank_owner" value="' . esc_attr($value) . '" />';
	}

	/**
	 * HTML for extra settings
	 */
	public function prehaff_bank_number_html()
	{
		$value = get_option('prehaff_bank_number', '');
		echo '<input type="text" id="prehaff_bank_number_id" name="prehaff_bank_number" value="' . esc_attr($value) . '" />';
	}

	/**
	 * HTML for extra settings
	 */
	public function prehaff_link_chat_facebook_html()
	{
		$value = get_option('prehaff_link_chat_facebook', '');
		echo '<input type="text" id="prehaff_link_chat_facebook_id" name="prehaff_link_chat_facebook" value="' . esc_attr($value) . '" />';
	}

	/**
	 * HTML for extra settings
	 */
	public function prehaff_link_chat_zalo_html()
	{
		$value = get_option('prehaff_link_chat_zalo', '');
		echo '<input type="text" id="prehaff_link_chat_zalo_id" name="prehaff_link_chat_zalo" value="' . esc_attr($value) . '" />';
	}
}


if (is_admin()) {
	new PREHAFF_Utilities();
}
