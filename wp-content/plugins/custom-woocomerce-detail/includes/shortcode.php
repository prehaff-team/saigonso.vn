<?php
class DHVC_Woo_Page_Shortcode
{

	protected $shop_single;
	protected $shop_thumbnails_size;
	protected $shortcodes;
	public function __construct()
	{
		$this->shortcodes = array(
			'dhvc_woo_product_page_images'								=> 'dhvc_woo_product_page_images_shortcode',
			'dhvc_woo_product_page_title'								=> 'dhvc_woo_product_page_title_shortcode',
			'dhvc_woo_product_page_rating'								=> 'dhvc_woo_product_page_rating_shortcode',
			'dhvc_woo_product_page_price'								=> 'dhvc_woo_product_page_price_shortcode',
			'dhvc_woo_product_page_excerpt'								=> 'dhvc_woo_product_page_excerpt_shortcode',
			'dhvc_woo_product_page_description'							=> 'dhvc_woo_product_page_description_shortcode',
			'dhvc_woo_product_page_additional_information'				=> 'dhvc_woo_product_page_additional_information',
			'dhvc_woo_product_page_add_to_cart'							=> 'dhvc_woo_product_page_add_to_cart_shortcode',
			'dhvc_woo_product_page_meta'								=> 'dhvc_woo_product_page_meta_shortcode',
			'dhvc_woo_product_page_sharing'								=> 'dhvc_woo_product_page_sharing_shortcode',
			'dhvc_woo_product_page_data_tabs'							=> 'dhvc_woo_product_page_data_tabs_shortcode',
			'dhvc_woo_product_page_reviews'								=> 'dhvc_woo_product_page_reviews_shortcode',
			'dhvc_woo_product_page_upsell'								=> 'dhvc_woo_product_page_upsell_shortcode',
			'dhvc_woo_product_page_related_products'					=> 'dhvc_woo_product_page_related_products_shortcode',
			'dhvc_woo_product_page_wishlist'							=> 'dhvc_woo_product_page_wishlist_shortcode',
			'dhvc_woo_product_page_product_category'           			=> 'product_category',
			'dhvc_woo_product_page_product_categories'        			=> 'product_categories',
			'dhvc_woo_product_page_products'                   			=> 'products',
			'dhvc_woo_product_page_product_recent_products'            	=> 'recent_products',
			'dhvc_woo_product_page_product_sale_products'              	=> 'sale_products',
			'dhvc_woo_product_page_product_best_selling_products'      	=> 'best_selling_products',
			'dhvc_woo_product_page_product_top_rated_products'         	=> 'top_rated_products',
			'dhvc_woo_product_page_product_featured_products'          	=> 'featured_products',
			'dhvc_woo_product_page_product_attribute'          			=> 'product_attribute',
			'dhvc_woo_product_page_shop_messages'              			=> 'shop_messages',
			'dhvc_woo_product_page_order_tracking' 						=> 'order_tracking',
			'dhvc_woo_product_page_cart'           						=> 'cart',
			'dhvc_woo_product_page_checkout'      						=> 'checkout',
			'dhvc_woo_product_page_my_account'     						=> 'my_account',
			'dhvc_woo_product_custom_field'								=> 'dhvc_woo_product_custom_field_shortcode',
			'dhvc_woo_product_breadcrumb'								=> 'breadcrumb',
			// 'prehaff_sidebar_stores'									=> 'prehaff_sidebar_stores_shortcode',
			'prehaff_show_comments'										=> 'prehaff_show_comments_shortcode',
			'prehaff_show_btns_order_call'								=> 'prehaff_show_buttons_order_and_call_shortcode',
		);
		if (apply_filters('dhvc_woocommerce_page_use_hook_before_override', false))
			add_action('dhvc_woocommerce_page_before_override', array(&$this, 'add_shortcode'));
		else
			add_action('template_redirect', array(&$this, 'add_shortcode'));
	}

	public function add_shortcode()
	{
		global $post;
		if ('product' == get_post_type($post)) {
			foreach ($this->shortcodes as $shortcode => $function) {
				add_shortcode($shortcode, array(&$this, $function));
			}
			if (class_exists('acf')) {
				add_shortcode('dhvc_woo_product_page_custom_field', array(&$this, 'dhvc_woo_product_page_custom_field_shortcode'));
			}

			if (defined('YITH_YWZM_DIR')) {
				remove_shortcode('dhvc_woo_product_page_images');
				add_shortcode('dhvc_woo_product_page_images', array(&$this, 'dhvc_woo_product_page_images_shortcode_custom'));
			}
		} else {
			foreach ($this->shortcodes as $shortcode => $function) {
				add_shortcode($shortcode, array(&$this, 'shortcode_error'));
			}
		}
	}

	public function shortcode_error($atts = '', $content = '', $tag = '')
	{
		return '<em style="color:red;display:block">Use shortcode "' . $tag . '" is wrong (Please view Product after assign Custom Template), to use plugin please see <a target="_blank" href="https://www.youtube.com/watch?v=DhqOQdR7K_8">Video</a><br><br></em>';
	}

	public function dhvc_woo_product_page_images_shortcode_custom($atts, $content = null)
	{
		global $product;
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		ob_start();
		if (!empty($el_class))
			echo '<div class="' . $el_class . '">';
		if (function_exists('dhwcpl_single_product')) {
			dhwcpl_single_product();
			dhwcpl_product_sale();
			dhwcpl_product_out_of_store();
		}

		$wc_get_template = function_exists('wc_get_template') ? 'wc_get_template' : 'woocommerce_get_template';
		$wc_get_template('single-product/product-image-magnifier.php', array(), '', YITH_YWZM_DIR . 'templates/');

		if (!empty($el_class))
			echo '</div>';
		return ob_get_clean();
	}

	public function get_shortcodes()
	{
		return $this->shortcodes;
	}

	public function dhvc_woo_product_page_custom_field_shortcode($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'field' => '',
			'el_class' => ''
		), $atts));
		if (empty($field)) {
			return '';
		}
		ob_start();
		echo '<div class="dhvc_woo_product_page_custom_field ' . $el_class . '">';
		//the_field ( $field );
		$value = get_field($field);
		//filter to custom display
		$value = apply_filters('dhvc_woo_product_page_custom_field_value', $value, $field);
		if (is_array($value)) {
			$value = @implode(', ', $value);
		}

		echo do_shortcode($value);

		echo '</div>';
		return ob_get_clean();
	}

	public function dhvc_woo_product_page_images_shortcode($atts, $content = null)
	{
		global $product;
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		ob_start();
		if (!empty($el_class))
			echo '<div class="' . $el_class . '">';
		if (function_exists('dhwcpl_single_product')) {
			dhwcpl_single_product();
			dhwcpl_product_sale();
			dhwcpl_product_out_of_store();
		}
		if (class_exists('JCKWooThumbs')) {
			$JCKWooThumbs = new JCKWooThumbs;
			$JCKWooThumbs->show_product_images();
		} else if (class_exists('WC_Product_Gallery_slider')) {
			$enabled = get_option('woocommerce_product_gallery_slider_enabled');
			$enabled_for_post   = get_post_meta($post->ID, '_woocommerce_product_gallery_slider_enabled', true);

			if (($enabled == 'yes' && $enabled_for_post !== 'no') || ($enabled == 'no' && $enabled_for_post == 'yes')) {
				WC_Product_Gallery_slider::setup_scripts_styles();
				WC_Product_Gallery_slider::show_product_gallery();
			}
		} else {
			woocommerce_show_product_sale_flash();
			woocommerce_show_product_images();
		}
		if (!empty($el_class))
			echo '</div>';
		return ob_get_clean();
	}

	public function dhvc_woo_product_page_title_shortcode($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		ob_start();
		if (!empty($el_class))
			echo '<div class="' . $el_class . '">';

		if (function_exists('mk_theme_options')) {
			?>
			<h1 itemprop="name" class="single_product_title entry-title"><?php the_title(); ?></h1>
		<?php
		} else {
			woocommerce_template_single_title();
		}
		if (!empty($el_class))
			echo '</div>';
		return ob_get_clean();
	}
	public function dhvc_woo_product_page_rating_shortcode($atts, $content = null)
	{
		global $post, $product;
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		ob_start();
		if (!empty($el_class))
			echo '<div class="' . $el_class . '">';
		if (function_exists('mk_theme_options')) {
			?>
			<?php
			$count   = $product->get_rating_count();
			$average = $product->get_average_rating();

			if ($count > 0) : ?>

				<div class="woocommerce-product-rating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
					<div class="star-rating" title="<?php printf(__('Rated %s out of 5', 'woocommerce'), $average); ?>">
						<span style="width:<?php echo (($average / 5) * 100); ?>%">
							<strong itemprop="ratingValue" class="rating"><?php echo esc_html($average); ?></strong> <?php _e('out of 5', 'woocommerce'); ?>
						</span>
					</div>
					<a href="#reviews" class="woocommerce-review-link" rel="nofollow">(<?php printf(_n('%s customer review', '%s customer reviews', $count, 'woocommerce'), '<span itemprop="ratingCount" class="count">' . $count . '</span>'); ?>)</a>
				</div>
			<?php
			endif;
		} else {
			woocommerce_template_single_rating();
		}
		if (!empty($el_class))
			echo '</div>';
		return ob_get_clean();
	}
	public function dhvc_woo_product_page_price_shortcode($atts, $content = null)
	{
		global $post, $product;
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		ob_start();
		if (!empty($el_class))
			echo '<div class="' . $el_class . '">';
		if (function_exists('mk_theme_options')) {
			?>
			<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">

				<div itemprop="price" class="mk-single-price"><?php echo $product->get_price_html(); ?></div>

				<meta itemprop="priceCurrency" content="<?php echo get_woocommerce_currency(); ?>" />
				<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />

			</div>
		<?php
		} else {
			woocommerce_template_single_price();
		}
		if (!empty($el_class))
			echo '</div>';
		return ob_get_clean();
	}
	public function dhvc_woo_product_page_excerpt_shortcode($atts, $content = null)
	{
		global $post, $product;
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		ob_start();
		if (!empty($el_class))
			echo '<div class="' . $el_class . '">';
		if (function_exists('mk_theme_options')) {
			?>
			<div itemprop="description">
				<?php echo apply_filters('woocommerce_short_description', $post->post_excerpt) ?>
			</div>
		<?php
		} else {
			woocommerce_template_single_excerpt();
		}
		if (!empty($el_class))
			echo '</div>';
		return ob_get_clean();
	}

	public function dhvc_woo_product_page_description_shortcode($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		ob_start();
		if (!empty($el_class))
			echo '<div class="' . $el_class . '">';

		the_content();

		if (!empty($el_class))
			echo '</div>';
		return ob_get_clean();
	}

	public function dhvc_woo_product_page_additional_information($atts, $content = null)
	{
		global $product, $post;
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		ob_start();
		if (!empty($el_class))
			echo '<div class="' . $el_class . '">';

		if ($product && ($product->has_attributes() || ($product->enable_dimensions_display() && ($product->has_dimensions() || $product->has_weight())))) {
			wc_get_template('single-product/tabs/additional-information.php');
		}

		if (!empty($el_class))
			echo '</div>';
		return ob_get_clean();
	}

	public function dhvc_woo_product_page_add_to_cart_shortcode($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		ob_start();
		if (!empty($el_class))
			echo '<div class="' . $el_class . '">';
		woocommerce_template_single_add_to_cart();
		if (!empty($el_class))
			echo '</div>';
		return ob_get_clean();
	}
	public function dhvc_woo_product_page_meta_shortcode($atts, $content = null)
	{
		global $post, $product;
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		ob_start();
		if (!empty($el_class))
			echo '<div class="' . $el_class . '">';

		if (function_exists('mk_theme_options')) {
			?>
			<div class="mk_product_meta">

				<?php do_action('woocommerce_product_meta_start'); ?>

				<?php
				$cat_count = sizeof(get_the_terms($post->ID, 'product_cat'));
				$tag_count = sizeof(get_the_terms($post->ID, 'product_tag'));

				if (wc_product_sku_enabled() && ($product->get_sku() || $product->is_type('variable'))) : ?>

					<span class="sku_wrapper"><?php _e('SKU:', 'woocommerce'); ?> <span class="sku" itemprop="sku"><?php echo ($sku = $product->get_sku()) ? $sku : __('N/A', 'woocommerce'); ?></span>.</span>

				<?php endif; ?>

				<?php echo $product->get_categories(', ', '<span class="posted_in">' . _n('Category:', 'Categories:', $cat_count, 'woocommerce') . ' ', '.</span>'); ?>

				<?php echo $product->get_tags(', ', '<span class="tagged_as">' . _n('Tag:', 'Tags:', $tag_count, 'woocommerce') . ' ', '.</span>'); ?>

				<?php do_action('woocommerce_product_meta_end'); ?>

			</div>
		<?php
		} else {
			woocommerce_template_single_meta();
		}
		if (!empty($el_class))
			echo '</div>';
		return ob_get_clean();
	}
	public function dhvc_woo_product_page_sharing_shortcode($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		ob_start();
		if (!empty($el_class))
			echo '<div class="' . $el_class . '">';
		if (function_exists('mk_theme_options')) {
			?>
			<ul class="woocommerce-social-share">
				<li><a class="facebook-share" data-title="<?php the_title(); ?>" data-url="<?php echo get_permalink(); ?>" href="#"><i class="mk-jupiter-icon-simple-facebook"></i></a></li>
				<li><a class="twitter-share" data-title="<?php the_title(); ?>" data-url="<?php echo get_permalink(); ?>" href="#"><i class="mk-moon-twitter"></i></a></li>
				<li><a class="googleplus-share" data-title="<?php the_title(); ?>" data-url="<?php echo get_permalink(); ?>" href="#"><i class="mk-jupiter-icon-simple-googleplus"></i></a></li>
				<li><a class="pinterest-share" data-image="<?php echo $image_src_array[0]; ?>" data-title="<?php echo get_the_title(); ?>" data-url="<?php echo get_permalink(); ?>" href="#"><i class="mk-jupiter-icon-simple-pinterest"></i></a></li>
			</ul>
		<?php
		} else {
			woocommerce_template_single_sharing();
		}
		if (!empty($el_class))
			echo '</div>';
		return ob_get_clean();
	}
	public function dhvc_woo_product_page_data_tabs_shortcode($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		ob_start();
		if (!empty($el_class))
			echo '<div class="' . $el_class . '">';
		woocommerce_output_product_data_tabs();
		if (!empty($el_class))
			echo '</div>';
		return ob_get_clean();
	}
	public function dhvc_woo_product_page_reviews_shortcode($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		ob_start();
		if (!empty($el_class))
			echo '<div class="' . $el_class . '">';
		if (comments_open()) {
			comments_template();
		}
		if (!empty($el_class))
			echo '</div>';
		return ob_get_clean();
	}
	public function dhvc_woo_product_page_related_products_shortcode($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'posts_per_page' => 4,
			'columns' => 4,
			'orderby' => 'date',
			'el_class' => ''
		), $atts));
		ob_start();
		if (!empty($el_class))
			echo '<div class="' . $el_class . '">';
		echo woocommerce_related_products($atts);
		if (!empty($el_class))
			echo '</div>';
		return ob_get_clean();
	}

	public function dhvc_woo_product_custom_field_shortcode($atts, $content = null)
	{
		global $post;
		extract(shortcode_atts(array(
			'key' => '',
			'label' => '',
			'el_class' => ''
		), $atts));

		$css_class = 'dhvc_woo_product-meta-field-' . $key
			. (strlen($el_class) ? ' ' . $el_class : '');
		if (strlen($label)) {
			$label_html = '<span class="dhvc_woo_product-meta-label">' . esc_html($label) . '</span>';
		}
		ob_start();
		if (!empty($key) && $value = get_post_meta($post->ID, $key, true)) :
			?>
			<div class="<?php echo esc_attr($css_class) ?>"><?php echo $label_html ?> <?php echo $value; ?></div>
		<?php
		endif;
		return ob_get_clean();
	}

	public function dhvc_woo_product_page_upsell_shortcode($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'posts_per_page' => 4,
			'columns' => 4,
			'orderby' => 'date',
			'el_class' => ''
		), $atts));
		ob_start();
		if (!empty($el_class))
			echo '<div class="' . $el_class . '">';
		woocommerce_upsell_display($posts_per_page, $columns, $orderby);
		if (!empty($el_class))
			echo '</div>';
		return ob_get_clean();
	}

	public function dhvc_woo_product_page_wishlist_shortcode($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		$output = '';
		$output .= '<div class="dhvc-woocommerce-page-wishlist ' . ($el_class ? $el_class : '') . '">';
		$output .= do_shortcode('[yith_wcwl_add_to_wishlist]');
		$output .= '</div>';
		return $output;
	}

	public function product_category($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		$output = '';
		if (!empty($el_class))
			$output .= '<div class="' . $el_class . '">';
		$output .= WC_Shortcodes::product_category($atts);
		if (!empty($el_class))
			$output .= '</div>';
		return $output;
	}
	public function product_categories($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		$output = '';
		if (!empty($el_class))
			$output .= '<div class="' . $el_class . '">';
		$output .= WC_Shortcodes::product_categories($atts);
		if (!empty($el_class))
			$output .= '</div>';
		return $output;
	}
	public function products($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		$output = '';
		if (!empty($el_class))
			$output .= '<div class="' . $el_class . '">';
		$output .= WC_Shortcodes::products($atts);
		if (!empty($el_class))
			$output .= '</div>';
		return $output;
	}
	public function recent_products($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		$output = '';
		if (!empty($el_class))
			$output .= '<div class="' . $el_class . '">';
		$output .= WC_Shortcodes::recent_products($atts);
		if (!empty($el_class))
			$output .= '</div>';
		return $output;
	}
	public function sale_products($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		$output = '';
		if (!empty($el_class))
			$output .= '<div class="' . $el_class . '">';
		$output .= WC_Shortcodes::sale_products($atts);
		if (!empty($el_class))
			$output .= '</div>';
		return $output;
	}
	public function best_selling_products($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		$output = '';
		if (!empty($el_class))
			$output .= '<div class="' . $el_class . '">';
		$output .= WC_Shortcodes::best_selling_products($atts);
		if (!empty($el_class))
			$output .= '</div>';
		return $output;
	}
	public function top_rated_products($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		$output = '';
		if (!empty($el_class))
			$output .= '<div class="' . $el_class . '">';
		$output .= WC_Shortcodes::top_rated_products($atts);
		if (!empty($el_class))
			$output .= '</div>';
		return $output;
	}
	public function featured_products($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		$output = '';
		if (!empty($el_class))
			$output .= '<div class="' . $el_class . '">';
		$output .= WC_Shortcodes::featured_products($atts);
		if (!empty($el_class))
			$output .= '</div>';
		return $output;
	}
	public function product_attribute($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		$output = '';
		if (!empty($el_class))
			$output .= '<div class="' . $el_class . '">';
		$output .= WC_Shortcodes::product_attribute($atts);
		if (!empty($el_class))
			$output .= '</div>';
		return $output;
	}
	public function shop_messages($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		$output = '';
		if (!empty($el_class))
			$output .= '<div class="' . $el_class . '">';
		$output .= WC_Shortcodes::shop_messages($atts);
		if (!empty($el_class))
			$output .= '</div>';
		return $output;
	}
	public function order_tracking($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		$output = '';
		if (!empty($el_class))
			$output .= '<div class="' . $el_class . '">';
		$output .= WC_Shortcodes::order_tracking($atts);
		if (!empty($el_class))
			$output .= '</div>';
		return $output;
	}
	public function cart($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		$output = '';
		if (!empty($el_class))
			$output .= '<div class="' . $el_class . '">';
		$output .= WC_Shortcodes::cart($atts);
		if (!empty($el_class))
			$output .= '</div>';
		return $output;
	}

	public function breadcrumb($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		$output = '';
		if (!empty($el_class))
			$output .= '<div class="' . $el_class . '">';
		$output .= woocommerce_breadcrumb();
		if (!empty($el_class))
			$output .= '</div>';
		return $output;
	}
	public function checkout($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		$output = '';
		if (!empty($el_class))
			$output .= '<div class="' . $el_class . '">';
		$output .= WC_Shortcodes::checkout($atts);
		if (!empty($el_class))
			$output .= '</div>';
		return $output;
	}
	public function my_account($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));
		$output = '';
		if (!empty($el_class))
			$output .= '<div class="' . $el_class . '">';
		$output .= WC_Shortcodes::my_account($atts);
		if (!empty($el_class))
			$output .= '</div>';
		return $output;
	}

	public function prehaff_sidebar_stores_shortcode($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));

		$stores = array(
			[
				"name" => "578 Đường 3/2 , P14 , Quận 10",
				"phone" => "0941334455",
				"phone_text" => "0941.33.44.55",
				"map_id" => 1
			],
			[
				"name" => "13 Nguyễn Phúc Nguyên , P10 , Quận 3",
				"phone" => "0941334455",
				"phone_text" => "0941.33.44.55",
				"map_id" => 2
			]
		);

		ob_start();
		echo '<div class="sidebar_stores ' . $el_class . '">';
		echo '<h4>Hệ thống cửa hàng</h4>';

		foreach ($stores as $store) {
			echo '<div class="detail-address-item">';
			echo '<div id="modal-map-' . $store['map_id'] . '" class="w3-modal w3-animate-zoom" onclick="this.style.display=\'none\'">';
			echo do_shortcode('[huge_it_maps id="' . $store['map_id'] . '"]');
			echo '</div>';
			echo '<p class="name">' . $store['name'] . '</p>';
			echo '<p class="hotline"><a href="tel:' . $store['phone'] . '">' . $store['phone_text'] . '</a> <a href="javascript:;" onclick="document.getElementById(\'modal-map-' . $store['map_id'] . '\').style.display=\'block\'" class="google-map-modal"><i class="fa fa-location-arrow" aria-hidden="true"></i> Chỉ đường</a></p>';
			echo '</div>';
		}

		echo '</div>';
		return ob_get_clean();
	}

	public function prehaff_show_comments_shortcode($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));

		ob_start();
		echo '<div class="comments-wrapper ' . $el_class . '">';
		if (is_singular() && post_type_supports(get_post_type(), 'comments')) {
			comments_template();
		}
		echo '</div>';
		return ob_get_clean();
	}

	public function prehaff_show_buttons_order_and_call_shortcode($atts, $content = null)
	{
		extract(shortcode_atts(array(
			'el_class' => ''
		), $atts));

		ob_start();
		echo '<div class="buttons-wrapper ' . $el_class . '">';
		echo '<div class="nm-row">';
		echo '<div class="col-sm-6 btn-left"><a href="'.get_option('prehaff_link_chat_zalo').'" target="_blank" class="btn btn-light-blue btn-full btn-small">';
		echo 'Tư vấn qua Zalo';
		echo '</a></div>';
		echo '<div class="col-sm-6 btn-right"><a href="'.get_option('prehaff_link_chat_facebook').'" target="_blank" class="btn btn-primary btn-full btn-small">';
		echo 'Tư vấn qua Facebook';
		echo '</a></div>';
		echo '<div class="col-sm-12 btn-order-fixing"><button class="btn btn-danger btn-full" id="open-modal-order" onclick="document.getElementById(\'modal-create-fixing-order\').style.display=\'block\'">';
		echo '<span class="main-line">Đặt lịch sửa chữa</span>';
		echo '<span>(Giảm thêm 5% tối đa 50.000 đ)</span>';
		echo '</button></div>';
		echo '</div>';

		echo '<div class="w3-row w3-section">';
		echo '<div class="w3-col call-me-input">';
		echo '<input class="w3-input w3-border" name="phone" type="number" placeholder="Số điện thoại">';
		echo '</div>';
		echo '<div class="w3-rest"><button class="btn btn-primary btn-full btn-rest" onclick="callMe();">Gọi lại cho tôi</button></div>';
		echo '</div>';

		echo '</div>';

		// echo js script
		echo '<script type="text/javascript">
		var $ = jQuery;
		var urlSite = window.location.origin;
		var urlCallMe = urlSite + "/wp-json/prehaff/v1/call-me";

		function callMe() {
			var params = {
				p: $("input[name=\"phone\"]").val()
			};
			$.get(urlCallMe, params).done(function(data) {
				if (data.status) {
					$("#modal-call-me .w3-container .w3-row-padding .w3-col p").text("Bạn đã gửi yêu cầu gọi lại thành công");
					$("#modal-call-me h2").text("Gửi yêu cầu gọi lại thành công");
					$("#modal-call-me header").removeClass("w3-red");
					$("#modal-call-me header").addClass("w3-teal");
					$("#modal-call-me footer").removeClass("w3-red");
					$("#modal-call-me footer").addClass("w3-teal");
				}else{
					$("#modal-call-me .w3-container .w3-row-padding .w3-col p").text("Gửi yêu cầu gọi lại không thành công");
					$("#modal-call-me h2").text("Gửi yêu cầu gọi lại không thành công");
					$("#modal-call-me header").removeClass("w3-teal");
					$("#modal-call-me header").addClass("w3-red");
					$("#modal-call-me footer").removeClass("w3-teal");
					$("#modal-call-me footer").addClass("w3-red");
				}
				$(\'#modal-call-me\').show();
			});
		}
	</script>';

		// echo modal success
		echo '<div class="order-form ' . $el_class . '">
			<div id="modal-call-me" class="w3-modal">
				<div class="w3-modal-content">
					<header class="w3-container w3-teal">
						<span onclick="document.getElementById(\'modal-call-me\').style.display=\'none\'" class="w3-button w3-display-topright">&times;</span>
						<h2>Gửi yêu cầu gọi lại thành công</h2>
					</header>
					<div class="w3-container">
						<div class="w3-row-padding">
							<div class="w3-col">
							<br />
								<p>Bạn đã gửi yêu cầu gọi lại thành công</p>
								<br />
							</div>
						</div>
					</div>
					<footer class="w3-container w3-teal">
						<p><button type="button" class="w3-btn w3-white w3-border w3-border-blue w3-round" style="width:120px" onclick="document.getElementById(\'modal-call-me\').style.display=\'none\'">Đóng</button></p>
					</footer>
				</div>
			</div>
		</div>';
		return ob_get_clean();
	}
}
