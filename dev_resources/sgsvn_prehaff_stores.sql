/*
Navicat MySQL Data Transfer

Source Server         : Localhost-3306
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : saigonso

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-08-04 20:51:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sgsvn_prehaff_stores
-- ----------------------------
DROP TABLE IF EXISTS `sgsvn_prehaff_stores`;
CREATE TABLE `sgsvn_prehaff_stores` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8mb4_unicode_ci,
  `phone` int(11) NOT NULL,
  `phone_text` text COLLATE utf8mb4_unicode_ci,
  `map_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of sgsvn_prehaff_stores
-- ----------------------------
INSERT INTO `sgsvn_prehaff_stores` VALUES ('1', '578 Đường 3/2 , P14 , Quận 10', '941334455', '0941.33.44.55', '1', '2019-08-04 20:46:53', '2019-08-04 20:46:53');
INSERT INTO `sgsvn_prehaff_stores` VALUES ('2', '13 Nguyễn Phúc Nguyên , P10 , Quận 3', '941334455', '0941.33.44.55', '2', '2019-08-04 20:47:15', '2019-08-04 20:47:15');
